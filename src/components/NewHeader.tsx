import React, { HTMLAttributes, ReactNode } from 'react'
import './NewHeader.scss'

export interface NewHeaderProps extends HTMLAttributes<HTMLDivElement> {
  children?: ReactNode
}
export const NewHeader = ({ children }: NewHeaderProps) => {
  return <div className="new-header">{children}</div>
}
