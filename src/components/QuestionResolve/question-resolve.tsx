import {
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  IconButton,
  Radio,
  RadioGroup,
} from '@material-ui/core'
import React, { HTMLAttributes, useState } from 'react'
import { IQuestion } from '../../interfaces/question.interface'
import './question-resolve.scss'

export interface QuestionResolveProps extends HTMLAttributes<HTMLDivElement> {
  question?: IQuestion
  getAlternative?: any
}

export const QuestionResolve = ({
  question = null,
  getAlternative,
  ...props
}: QuestionResolveProps) => {
  const handleChange = (event) => {
    getAlternative(event.target.value, question?.numQuestion)
  }

  return (
    <div className="question-resolve">
      <div className="question-resolve__header">
        <div className="question-resolve__header-label">
          Pregunta {question?.numQuestion}
        </div>
      </div>
      <div className="question-resolve__statement">{question?.statement}</div>
      <div className="question-resolve__radiobutton">
        <FormControl component="fieldset">
          <FormLabel
            component="legend"
            className="question-resolve__radiobutton-label"
          >
            Alternativas
          </FormLabel>
          <RadioGroup aria-label="quizView" name="radio-buttons-group">
            {question?.alternatives.map((a) => (
              <FormControlLabel
                value={a.alternative.toString()}
                control={<Radio />}
                label={a.alternative}
                onChange={handleChange}
              />
            ))}
          </RadioGroup>
        </FormControl>
      </div>
    </div>
  )
}
