import {
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  IconButton,
  Radio,
  RadioGroup,
} from '@material-ui/core'
import { Add, Delete, Save } from '@material-ui/icons'
import React, { HTMLAttributes, useState } from 'react'
import ReactQuill from 'react-quill'
import { IQuestion } from '../../interfaces/question.interface'
import './question.scss'

export interface QuestionProps extends HTMLAttributes<HTMLDivElement> {
  numQuestion?: number
  trashQuestion?: any
  question?: IQuestion
  addQuestion?: any
}

export const Question = ({
  numQuestion = 0,
  trashQuestion,
  question,
  addQuestion,
  ...props
}: QuestionProps) => {
  const [convertedText, setConvertedText] = useState(question.statement)

  return (
    <div className="question">
      <div className="question__header">
        <div className="question__header-label">Pregunta {numQuestion}</div>
        <Button
          variant="contained"
          onClick={() => addQuestion(convertedText, numQuestion)}
          startIcon={<Add />}
          className="question__header-label-add"
        >
          Confirmar Pregunta
        </Button>
        <div className="question__header-delete">
          <IconButton
            aria-label="delete"
            onClick={() => trashQuestion(numQuestion - 1)}
          >
            <Delete />
          </IconButton>
        </div>
      </div>
      <ReactQuill
        theme="snow"
        value={convertedText}
        onChange={setConvertedText}
        style={{ width: '1300px', height: '52px' }}
        className="question-reactquill"
      />
      <div className="question-radiobutton">
        <FormControl component="fieldset">
          <FormLabel component="legend">Elige</FormLabel>
          <RadioGroup aria-label="quiz" name="radio-buttons-group">
            <FormControlLabel
              value="1"
              control={<Radio />}
              label="1 Totalmente desacuerdo"
              disabled
            />
            <FormControlLabel
              value="2"
              control={<Radio />}
              label="2 En desacuerdo"
              disabled
            />
            <FormControlLabel
              value="3"
              control={<Radio />}
              label="3 Ni de acuerdo ni en desacuerdo"
              disabled
            />
            <FormControlLabel
              value="4"
              control={<Radio />}
              label="4 De acuerdo"
              disabled
            />
            <FormControlLabel
              value="5"
              control={<Radio />}
              label="5 Totalmente de acuerdo"
              disabled
            />
          </RadioGroup>
        </FormControl>
      </div>
    </div>
  )
}
