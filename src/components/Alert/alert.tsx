import { ReactNode } from 'react'
import {
  transitions,
  positions,
  Provider as ReactAlertProvider,
} from 'react-alert'

import { CustomAlertTemplate } from './custom-alert-template'

export interface AlertProviderProps {
  children: ReactNode
}

export const AlertProvider = ({ children }: AlertProviderProps) => {
  const alertOptions = {
    position: positions.TOP_RIGHT,
    timeout: 3000,
    offset: '32px',
    transition: transitions.FADE,
  }

  return (
    // eslint-disable-next-line
    <ReactAlertProvider template={CustomAlertTemplate} {...alertOptions}>
      {children}
    </ReactAlertProvider>
  )
}
