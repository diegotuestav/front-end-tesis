import React from 'react'
import {
  InfoIcon,
  XCircleIcon,
  CheckCircleIcon,
} from '@pluralsight/ps-design-system-icon'

// TODO: Define alerts

export interface AlertMessageProps {
  title: string
  message: string
  type: string
}

export const AlertMessage = ({ title, message, type }: AlertMessageProps) => {
  // eslint-disable-next-line
  const Icon = ({ className = '' }) => {
    // eslint-disable-next-line
    switch (type) {
      case 'info':
        // eslint-disable-next-line
        return <InfoIcon className={className} size={'medium'} />
      case 'error':
        // eslint-disable-next-line
        return <XCircleIcon className={className} size={'medium'} />
      case 'success':
        // eslint-disable-next-line
        return <CheckCircleIcon className={className} size="medium" />
    }
    return <></>
  }
  return (
    <div className={`message ${type}`}>
      <Icon className="message_icon" />
      <div className="message_body">
        <h6>{title}</h6>
        <p>
          <small>{message}</small>
        </p>
      </div>
    </div>
  )
}
