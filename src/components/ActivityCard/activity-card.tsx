import { Button, IconButton } from '@material-ui/core'
import { Check, GolfCourse } from '@material-ui/icons'
import { useDispatch, useSelector } from 'react-redux'
import React, { HTMLAttributes, useState } from 'react'
import { IActivity } from '../../interfaces/activity.interface'
import './activity-card.scss'
import { activityLoadAction } from '../../store/activity/actions/load.action'

export interface ActivityCardProps extends HTMLAttributes<HTMLDivElement> {
  isDone?: boolean
  activity?: IActivity
  onClickView?: any
}

export const ActivityCard = ({
  isDone = false,
  activity = null,
  onClickView,
  ...props
}: ActivityCardProps) => {
  const dispatch = useDispatch()
  const viewActivity = () => {
    dispatch(activityLoadAction.request(activity?.id))
    onClickView()
  }
  return (
    <Button className="activity-card" onClick={viewActivity}>
      {isDone ? (
        <div className="activity-card-done"></div>
      ) : (
        <div className="activity-card-progress"></div>
      )}
      <div className="activity-card__content">
        <h1 className="activity-card__content-title">{activity?.title}</h1>
      </div>
    </Button>
  )
}
