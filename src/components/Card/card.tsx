import { Button, IconButton } from '@material-ui/core'
import { Check, GolfCourse } from '@material-ui/icons'
import React, { HTMLAttributes, useState } from 'react'
import './card.scss'

export interface CardProps extends HTMLAttributes<HTMLDivElement> {
  isDone?: boolean
  macroActivity?: string
  onClickView?: any
}

export const Card = ({
  isDone = false,
  macroActivity = '',
  onClickView,
  ...props
}: CardProps) => {
  return (
    <Button className="card" onClick={onClickView}>
      {isDone ? (
        <div className="card-done"></div>
      ) : (
        <div className="card-progress"></div>
      )}
      <div className="card__content">
        <h1 className="card__content-title">{macroActivity}</h1>
      </div>
    </Button>
  )
}
