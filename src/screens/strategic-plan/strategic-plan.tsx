import { Button } from '@material-ui/core'
import React, { useEffect, useState, useRef } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import MaterialTable from 'material-table'
import ReactHtmlParser from 'react-html-parser'
import { PDFExport } from '@progress/kendo-react-pdf'
import './strategic-plan.scss'
import { IState } from '../../store/reducers'
import { StrategicPlanGetAction } from '../../store/strategic-plan/actions/get.action'
import { stakeholdersLoadAction } from '../../store/stakeholders/actions/load.action'
import { sponsorshipLoadAction } from '../../store/sponsorship/actions/load.action'

const StrategicPlan = () => {
  const pdfExportComponent = useRef(null)
  const handleExportWithComponent = (event) => {
    pdfExportComponent.current.save()
  }
  const dispatch = useDispatch()
  const history = useHistory()
  const [sponsorName, setSponsorName] = useState('')
  const [sponsorLastName, setSponsorLastName] = useState('')
  const [sponsorCharge, setSponsorCharge] = useState('')
  const [sponsorEmail, setSponsorEmail] = useState('')
  const [sponsorPhone, setSponsorPhone] = useState('')

  const [comiteName, setComiteName] = useState('')
  const [comiteLastName, setComiteLastName] = useState('')
  const [comiteCharge, setComiteCharge] = useState('')
  const [comiteEmail, setComiteEmail] = useState('')
  const [comitePhone, setComitePhone] = useState('')

  const [isComite, setIsComite] = useState(false)
  const { listStrategicPlans, strategicPlan } = useSelector(
    (state: IState) => state.strategicPlan
  )
  const { stakeholders } = useSelector((state: IState) => state.stakeholder)
  const { sponsorship } = useSelector((state: IState) => state.sponsorship)

  useEffect(() => {
    if (sponsorship.length > 1) setIsComite(true)
    if (sponsorship) {
      const sponsor = sponsorship.filter((s) => s.role === 'Patrocinador')
      setSponsorName(sponsor[0]?.names)
      setSponsorLastName(sponsor[0]?.lastnames)
      setSponsorCharge(sponsor[0]?.charge)
      setSponsorEmail(sponsor[0]?.email)
      setSponsorPhone(sponsor[0]?.phone)

      const comiteSponsor = sponsorship.filter(
        (s) => s.role === 'Coordinador de cómite de patrocinio'
      )
      setComiteName(comiteSponsor[0]?.names)
      setComiteLastName(comiteSponsor[0]?.lastnames)
      setComiteCharge(comiteSponsor[0]?.charge)
      setComiteEmail(comiteSponsor[0]?.email)
      setComitePhone(comiteSponsor[0]?.phone)
    }
  }, [sponsorship])

  useEffect(() => {
    dispatch(sponsorshipLoadAction.request(strategicPlan?.id))
  }, [dispatch, sponsorshipLoadAction, strategicPlan])

  const columnasSponsor = [
    {
      title: 'Nombre(s)',
      field: 'names',
    },
    {
      title: 'Apellido(s)',
      field: 'lastnames',
    },
    {
      title: 'Correo Electrónico',
      field: 'email',
    },
    {
      title: 'Teléfono',
      field: 'phone',
    },
    {
      title: 'Cargo',
      field: 'charge',
    },
  ]

  const columnasStakeholder = [
    {
      title: 'Nombre(s)',
      field: 'names',
    },
    {
      title: 'Apellido(s)',
      field: 'lastnames',
    },
    {
      title: 'Perfil',
      field: 'profile',
    },
    {
      title: 'Adhesión al cambio',
      field: 'adhesion',
    },
    {
      title: 'Grupo',
      field: 'group',
    },
  ]

  useEffect(() => {
    if (strategicPlan)
      dispatch(StrategicPlanGetAction.request(strategicPlan?.id))
  }, [])

  useEffect(() => {
    dispatch(stakeholdersLoadAction.request(strategicPlan?.id))
  }, [dispatch, strategicPlan])

  const dataStakeholder = stakeholders.map((u) => ({
    names: u.names,
    lastnames: u.lastnames,
    profile: u.profile,
    adhesion: u.adhesion,
    group: u.collective,
  }))

  const dataSponsor = sponsorship
    .filter((u) => u.role === 'Miembro de cómite de patrocinio')
    .map((u) => ({
      names: u.names,
      lastnames: u.lastnames,
      email: u.email,
      phone: u.phone,
      charge: u.charge,
    }))

  useEffect(() => {
    if (!strategicPlan)
      dispatch(StrategicPlanGetAction.request(listStrategicPlans[0]?.id))
  }, [dispatch, listStrategicPlans])

  return (
    <div>
      <div className="strategic-plan">
        <div className="strategic-plan-header">
          <div className="strategic-plan-title">
            Plan Estratégico de Gestión del Cambio
          </div>
          <Button
            variant="outlined"
            className="strategic-plan-export-button"
            onClick={handleExportWithComponent}
          >
            Exportar
          </Button>
        </div>
        <PDFExport ref={pdfExportComponent} margin="2cm" paperSize="A3">
          <div className="strategic-plan-content">
            <div className="strategic-plan-sponsor">
              <h1>Patrocinio</h1>
              <h3>Patrocinador del proyecto</h3>
              <div className="strategic-plan-sponsor-member">
                <div className="strategic-plan-sponsor-member-section">
                  <h4 className="strategic-plan-sponsor-member-section-name">
                    Nombres:
                  </h4>
                  <h5 className="strategic-plan-sponsor-member-section-textfield">
                    {sponsorName}
                  </h5>
                  <h4 className="strategic-plan-sponsor-member-section-lastname">
                    Apellidos:
                  </h4>
                  <h5 className="strategic-plan-sponsor-member-section-textfield">
                    {sponsorLastName}
                  </h5>
                  <h4 className="strategic-plan-sponsor-member-section-charge">
                    Cargo:
                  </h4>
                  <h5 className="strategic-plan-sponsor-member-section-textfield">
                    {sponsorCharge}
                  </h5>
                </div>
                <div className="strategic-plan-sponsor-member-section">
                  <h4 className="strategic-plan-sponsor-member-section-email">
                    Correo Electrónico:
                  </h4>
                  <h5 className="strategic-plan-sponsor-member-section-textfield">
                    {sponsorEmail}
                  </h5>
                  <h4 className="strategic-plan-sponsor-member-section-phone">
                    Teléfono:
                  </h4>
                  <h5 className="strategic-plan-sponsor-member-section-textfield">
                    {sponsorPhone}
                  </h5>
                </div>
              </div>
              {isComite ? (
                <div>
                  <h3>Coordinador de cómite de patrocinio</h3>
                  <div className="strategic-plan-sponsor-member">
                    <div className="strategic-plan-sponsor-member-section">
                      <h4 className="strategic-plan-sponsor-member-section-name">
                        Nombres:
                      </h4>
                      <h5 className="strategic-plan-sponsor-member-section-textfield">
                        {comiteName}
                      </h5>
                      <h4 className="strategic-plan-sponsor-member-section-lastname">
                        Apellidos:
                      </h4>
                      <h5 className="strategic-plan-sponsor-member-section-textfield">
                        {comiteLastName}
                      </h5>
                      <h4 className="strategic-plan-sponsor-member-section-charge">
                        Cargo:
                      </h4>
                      <h5 className="strategic-plan-sponsor-member-section-textfield">
                        {comiteCharge}
                      </h5>
                    </div>
                    <div className="strategic-plan-sponsor-member-section">
                      <h4 className="strategic-plan-sponsor-member-section-email">
                        Correo Electrónico:
                      </h4>
                      <h5 className="strategic-plan-sponsor-member-section-textfield">
                        {comiteEmail}
                      </h5>
                      <h4 className="strategic-plan-sponsor-member-section-phone">
                        Teléfono:
                      </h4>
                      <h5 className="strategic-plan-sponsor-member-section-textfield">
                        {comitePhone}
                      </h5>
                    </div>
                  </div>
                  <h3>Cómite de patrocinio</h3>
                  <MaterialTable
                    columns={columnasSponsor}
                    title=""
                    data={dataSponsor}
                    options={{
                      actionsColumnIndex: -1,
                    }}
                  />
                </div>
              ) : (
                <div></div>
              )}
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Impactos Organizacionel Preliminares</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[0]?.blankSpace1)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Visión</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[2]?.blankSpace1)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Propósito y Objetivo</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[2]?.blankSpace2)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Metas y Métricas</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[1]?.blankSpace2)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Elementos de la cultura organizacional</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[4]?.blankSpace1)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Madurez para lidiar con pérdidas</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[8]?.blankSpace1)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Confianza</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[8]?.blankSpace2)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Factores de antagonismo</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[4]?.blankSpace2)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Factores de compromiso</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[4]?.blankSpace3)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Mapa de Stakeholders</h1>
              <MaterialTable
                columns={columnasStakeholder}
                title=""
                data={dataStakeholder}
                options={{
                  actionsColumnIndex: -1,
                }}
              />
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Mapa de riesgos inherentes al factor humano</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[4]?.blankSpace4)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Abordaje de Gestión del Cambio</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[9]?.blankSpace3)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Roles y Responsabilidades</h1>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Estructura de gestión del proyecto</h1>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Plan de asignación y desarrollo del equipo del proyecto</h1>
              <h3>Perfil técnico del equipo del proyecto</h3>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[7]?.blankSpace1)}
              </h2>
              <h3>Perfil comportamental del equipo del proyecto</h3>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[7]?.blankSpace2)}
              </h2>
              <h3>Entrenamientos previos para preparar al equipo</h3>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[7]?.blankSpace3)}
              </h2>
              <h3>Plan inicial de entrenamiento</h3>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[7]?.blankSpace4)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Identidad</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[2]?.blankSpace3)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Plan de comunicaciones ordinarias</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[1]?.blankSpace1)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Ambiente físico</h1>
              <h3>Necesidades, inversiones y beneficios</h3>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[6]?.blankSpace1)}
              </h2>
              <h3>Efectos en el equipo</h3>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[6]?.blankSpace2)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Plan de Acción</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[9]?.blankSpace1)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Estrategia de sostenimiento de los cambios</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(strategicPlan.macroActivities[9]?.blankSpace2)}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Kick-off</h1>
              <h3>Agenda</h3>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(
                  strategicPlan.macroActivities[10]?.blankSpace1
                )}
              </h2>
              <h3>Objetivos</h3>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(
                  strategicPlan.macroActivities[10]?.blankSpace2
                )}
              </h2>
              <h3>Inversiones previstas</h3>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(
                  strategicPlan.macroActivities[10]?.blankSpace3
                )}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Quick-Wins</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(
                  strategicPlan.macroActivities[11]?.blankSpace1
                )}
              </h2>
            </div>
            <div className="strategic-plan-sponsor">
              <h1>Presupuesto</h1>
              <h2 className="strategic-plan-text">
                {ReactHtmlParser(
                  strategicPlan.macroActivities[11]?.blankSpace2
                )}
              </h2>
            </div>
          </div>
        </PDFExport>
      </div>
    </div>
  )
}
export default StrategicPlan
