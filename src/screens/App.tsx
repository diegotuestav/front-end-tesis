import React from 'react'
import { Provider as ReduxProvider } from 'react-redux'
import { AppRoot } from './app-root'
import Store from '../store'
import { AuthProvider } from '../contexts/auth-context'

const App = () => {
  return (
    <ReduxProvider store={Store}>
      <AuthProvider>
        <AppRoot />
      </AuthProvider>
    </ReduxProvider>
  )
}

export default App
