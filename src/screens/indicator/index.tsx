import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { Path } from '../../common/constants/path.constants'
import { LazyIndicators, LazyQuiz } from '../lazy'

const Indicator = () => {
  return (
    <Switch>
      <Route path={Path.INDICATORS} exact component={LazyIndicators} />
      <Route path={Path.QUIZ} exact component={LazyQuiz} />
    </Switch>
  )
}
export default Indicator
