import { Button, TextField } from '@material-ui/core'
import { Add, ArrowBackIos, Save, Visibility } from '@material-ui/icons'
import { useHistory, useParams } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import { useDispatch, useSelector } from 'react-redux'
import { useAlert } from 'react-alert'
import { Question } from '../../../components/Question/question'
import { IState } from '../../../store/reducers'
import {
  IQuestion,
  QuestionModel,
} from '../../../interfaces/question.interface'
import { quizLoadAction } from '../../../store/quiz/actions/load.action'
import './quiz.scss'
import { updateQuizAction } from '../../../store/quiz/actions/update.actions'
import { AlertMessage } from '../../../components/Alert/alert-messages'

const Quiz = () => {
  const dispatch = useDispatch()
  const quiz = useSelector((state: IState) => state.quiz.quiz)
  const updated = useSelector((state: IState) => state.quiz.updated)
  const [title, setTitle] = useState('')
  const [introduction, setIntroduction] = useState('')
  const [questions, setQuestions] = useState<IQuestion[]>([])
  const [questionsNumber, setQuestionsNumber] = useState(0)
  const [questionsAdd, setQuestionsAdd] = useState<IQuestion[]>([])
  const history = useHistory()
  const params = useParams()

  useEffect(() => {
    if (quiz) {
      setTitle(quiz.title)
      setIntroduction(quiz.introduction)
      setQuestions(quiz.questions)
      setQuestionsNumber(quiz.questions.length)
      setQuestionsAdd(quiz.questions)
    }
  }, [quiz])

  const handleChangeTitle = (event) => {
    setTitle(event.target.value)
  }

  const saveQuestion = (statementQuestion: string, numQuestion: number) => {
    setQuestionsAdd((q) => [
      ...q,
      new QuestionModel(-1, statementQuestion, numQuestion, 5, [
        {
          alternative: '1 Totalmente Desacuerdo',
          isCorrect: true,
        },
        {
          alternative: '2 En Desacuerdo',
          isCorrect: true,
        },
        {
          alternative: '3 Ni deacuerdo ni en desacuerdo',
          isCorrect: true,
        },
        {
          alternative: '4 De acuerdo',
          isCorrect: true,
        },
        {
          alternative: '5 Totalmente de acuerdo',
          isCorrect: true,
        },
      ]),
    ])
  }

  const addQuestion = () => {
    setQuestions((q) => [...q, new QuestionModel(-1, '', -1, null)])
  }
  /*
  useEffect(() => {
    if (updated) {
      alert.show(
        <AlertMessage
          title="Categoría creada"
          message="La categoría se ha creado satisfactoriamente."
          type="success"
        />
      )
    }
  }, [updated, alert])
*/
  const trashQuestion = (index) => {
    setQuestions((q) => [...q.slice(0, index), ...q.slice(index + 1)])
  }

  const onClickView = () => {
    // eslint-disable-next-line
    history.push(`/indicators/${params['id']}/quiz/view`)
  }

  useEffect(() => {
    // eslint-disable-next-line
    dispatch(quizLoadAction.request(params['id']))
  }, [dispatch, params])

  const onClickBack = () => {
    history.go(-1)
  }

  const onSaveQuiz = () => {
    dispatch(
      updateQuizAction.request({
        // eslint-disable-next-line
        id_indicator: params['id'],
        title,
        introduction,
        questions: questionsAdd,
      })
    )
  }

  return (
    <div>
      <div className="quiz">
        <div className="quiz__back">
          <Button
            size="small"
            variant="text"
            startIcon={<ArrowBackIos />}
            disableElevation
            className="quiz__back-button"
            onClick={() => onClickBack()}
          >
            Atrás
          </Button>
        </div>
        <div className="quiz__title">Creación de Encuesta</div>
        <div className="quiz__content">
          <div className="quiz__content__quiz">
            <div className="quiz__content__quiz__title">
              <div className="quiz__label">Título de Encuesta:</div>
              <div className="quiz__content__quiz__title-textfield">
                <TextField
                  fullWidth
                  value={title}
                  id="fullWidth"
                  onChange={handleChangeTitle}
                />
              </div>
            </div>
            <div className="quiz__content__quiz__intro">
              <div className="quiz__label">Introducción</div>
              <ReactQuill
                theme="snow"
                value={introduction}
                onChange={setIntroduction}
                style={{ width: '1300px', height: '180px' }}
                className="quiz__content__quiz__intro-reactquill"
              />
            </div>
            <div className="quiz__content__quiz__questions">
              {questions.map((q, index) => (
                <Question
                  numQuestion={index + 1}
                  trashQuestion={trashQuestion}
                  addQuestion={saveQuestion}
                  question={q}
                />
              ))}
            </div>
            <Button
              variant="contained"
              onClick={addQuestion}
              startIcon={<Add />}
              className="quiz__content__quiz-add"
            >
              Agregar
            </Button>
          </div>
          <div className="quiz__content__actions">
            <div className="quiz__content__actions__save">
              <Button
                variant="contained"
                startIcon={<Save />}
                className="quiz__content__actions__save-button"
                onClick={onSaveQuiz}
              >
                GUARDAR
              </Button>
            </div>

            <div className="quiz__content__actions__visualize">
              <Button
                variant="contained"
                startIcon={<Visibility />}
                className="quiz__content__actions__visualize-button"
                onClick={() => onClickView()}
              >
                VISUALIZAR
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Quiz
