import {
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  TextField,
} from '@material-ui/core'
import {
  Add,
  ArrowBackIos,
  QuestionAnswerSharp,
  Save,
  Visibility,
} from '@material-ui/icons'
import React, { useEffect, useState } from 'react'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import { useHistory, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { IState } from '../../../../store/reducers'

import { quizLoadAction } from '../../../../store/quiz/actions/load.action'

import './quiz-resolved.scss'
import { QuestionResolve } from '../../../../components/QuestionResolve/question-resolve'
import { takeResultsAction } from '../../../../store/indicators/actions/take-results.action'

export const QuizResolved = () => {
  const dispatch = useDispatch()
  const { quiz } = useSelector((state: IState) => state.quiz)
  const params = useParams()
  const [getAnswers, setGetAnswers] = useState<
    {
      numPregunta: number
      answer: string
    }[]
  >([])

  useEffect(() => {
    // eslint-disable-next-line
    dispatch(quizLoadAction.request(params['id']))
  }, [dispatch])

  useEffect(() => {
    setGetAnswers(
      quiz?.questions.map((question) => ({
        numPregunta: question.numQuestion,
        answer: '',
      }))
    )
  }, [quiz])

  const getAlternatives = (alternative: string, numQuestion: number) => {
    const answerX = getAnswers.map((a, index) => {
      if (a.numPregunta === numQuestion) {
        return { numPregunta: numQuestion, answer: alternative }
      }
      return a
    })
    setGetAnswers(answerX)
  }

  const sendAnswers = () => {
    if (getAnswers.find((item) => item.answer === '')) console.log('Alerta')
    else
      dispatch(
        takeResultsAction.request({
          // eslint-disable-next-line
          idIndicator: params['id'],
          answers: getAnswers.map((a) => {
            return a.answer
          }),
        })
      )
  }

  return (
    <div>
      <div className="quiz-resolved">
        <div className="quiz-resolved__content">
          <div className="quiz-resolved__content-title">{quiz?.title}</div>
          <div className="quiz-resolved__content-intro">
            {quiz?.introduction}
          </div>
          <div className="quiz-resolved__content-questions">
            {quiz?.questions.map((q) => (
              <QuestionResolve question={q} getAlternative={getAlternatives} />
            ))}
          </div>
        </div>
        <div className="quiz-resolved__actions">
          <div className="quiz-resolved__actions__save">
            <Button
              variant="contained"
              className="quiz-resolved__actions__save-button"
              onClick={sendAnswers}
            >
              FINALIZAR ENCUESTA
            </Button>
          </div>
        </div>
      </div>
    </div>
  )
}
