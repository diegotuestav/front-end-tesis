import { Button, TextField } from '@material-ui/core'
import { Add, ArrowBackIos, Save, Visibility } from '@material-ui/icons'
import React, { useEffect, useState } from 'react'
import ReactQuill from 'react-quill'
import ReactHtmlParser from 'react-html-parser'
import 'react-quill/dist/quill.snow.css'
import { useHistory, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Question } from '../../../../components/Question/question'
import { IState } from '../../../../store/reducers'
import {
  IQuestion,
  QuestionModel,
} from '../../../../interfaces/question.interface'
import { quizLoadAction } from '../../../../store/quiz/actions/load.action'

import './quiz-view.scss'
import { QuestionResolve } from '../../../../components/QuestionResolve/question-resolve'

const QuizView = () => {
  const dispatch = useDispatch()
  const { quiz } = useSelector((state: IState) => state.quiz)
  const history = useHistory()
  const params = useParams()
  useEffect(() => {
    // eslint-disable-next-line
    dispatch(quizLoadAction.request(params['id']))
  }, [dispatch])

  const onClickBack = () => {
    history.go(-1)
  }

  return (
    <div>
      <div className="quiz-view">
        <div className="quiz-view__back">
          <Button
            size="small"
            variant="text"
            startIcon={<ArrowBackIos />}
            disableElevation
            className="indicator-option"
            onClick={() => onClickBack()}
          >
            Atrás
          </Button>
        </div>
        <div className="quiz-view__content">
          <div className="quiz-view__content-title">{quiz?.title}</div>
          <div className="quiz-view__content-intro">
            {ReactHtmlParser(quiz?.introduction)}
          </div>
          <div className="quiz-view__content-questions">
            {quiz?.questions.map((q) => (
              <QuestionResolve question={q} />
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

export default QuizView
