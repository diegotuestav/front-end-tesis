import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, IconButton } from '@material-ui/core'
import { Edit, HelpOutline } from '@material-ui/icons'
import { useHistory, useParams } from 'react-router-dom'
import { indicatorsLoadAction } from '../../../store/indicators/actions/load.action'
import { IState } from '../../../store/reducers'
import './indicators.scss'
import { ModalEditGoal } from './edit-goal/edit-goal'
import { ModalSeeVariables } from './see-variables/see-variables'

const Indicators = () => {
  const dispatch = useDispatch()
  const [viewModalEditGoal, setViewModalEditGoal] = useState(false)
  const [viewModalSeeVariables, setViewModalSeeVariables] = useState(false)
  const { indicators } = useSelector((state: IState) => state.indicators)
  const [indicatorSelected, setIndicatorSelected] = useState(indicators[0])
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const history = useHistory()

  const onClickIndicatorSelected = (numIndicator) => {
    setIndicatorSelected(indicators[numIndicator])
  }

  useEffect(() => {
    dispatch(indicatorsLoadAction.request(strategicPlan?.id))
  }, [dispatch, strategicPlan])

  const onClickQuiz = () => {
    history.push(`/indicators/${indicatorSelected?.id}/quiz`)
  }

  const onClickQuiz1 = () => {
    history.push(`/indicators/${1}/quiz`)
  }

  return (
    <div>
      <div className="indicators">
        <div className="indicators__title">Monitoreo de Indicadores</div>
        <div className="indicators__content">
          <div className="indicators__content__menu">
            <div className="indicators__content-subtitle">INDICADORES</div>
            <div className="indicators__content__menu-indicator">
              <Button
                size="small"
                variant="text"
                disableElevation
                className="indicator-option"
                onClick={() => onClickIndicatorSelected(0)}
              >
                Interesados con baja satisfacción laboral
              </Button>
            </div>
            <div className="indicators__content__menu-indicator-black">
              <Button
                size="small"
                variant="text"
                disableElevation
                className="indicator-option"
                onClick={() => onClickIndicatorSelected(1)}
              >
                Interesados con desconocimiento del objetivo del proyecto
              </Button>
            </div>
            <div className="indicators__content__menu-indicator-black">
              <Button
                size="small"
                variant="text"
                disableElevation
                className="indicator-option"
                onClick={() => onClickIndicatorSelected(2)}
              >
                Tasa de absentismo laboral
              </Button>
            </div>
            <div className="indicators__content__menu-indicator-black">
              <Button
                size="small"
                variant="text"
                disableElevation
                className="indicator-option"
                onClick={() => onClickIndicatorSelected(3)}
              >
                Tasa de equipos con demoras en entregar objetivos parciales
              </Button>
            </div>
            <div className="indicators__content__menu-indicator-black">
              <Button
                size="small"
                variant="text"
                disableElevation
                className="indicator-option"
                onClick={() => onClickIndicatorSelected(4)}
              >
                Renuncias voluntarias
              </Button>
            </div>
            <div className="indicators__content__menu-indicator-black">
              <Button
                size="small"
                variant="text"
                disableElevation
                className="indicator-option"
                onClick={() => onClickIndicatorSelected(5)}
              >
                Promedio del conocimiento logrado con entrenamiento
              </Button>
            </div>
          </div>
          {indicatorSelected ? (
            <div className="indicators__content__details">
              <div className="indicators__content__details__first">
                <div className="indicators__content-subtitle">DESCRIPCIÓN</div>
                <div className="indicators__content__details__first-text">
                  {indicatorSelected.description}
                </div>
              </div>
              <div className="indicators__content__details__second">
                <div className="indicators__content__details__second__formula">
                  <div className="flex">
                    <div className="indicators__content-subtitle">FÓRMULA</div>
                    <IconButton
                      aria-label="variables"
                      onClick={() => setViewModalSeeVariables(true)}
                    >
                      <HelpOutline className="indicators__content-subtitle-icon" />
                    </IconButton>
                  </div>
                  <div className="indicators__content__details__second__formula-text">
                    {indicatorSelected.formula}
                  </div>
                </div>
                <div className="indicators__content__details__second__measure">
                  <div className="indicators__content-subtitle">
                    UNIDAD DE MEDICIÓN
                  </div>
                  <div className="indicators__content__details__second__measure-text">
                    {indicatorSelected.measureUnit}
                  </div>
                </div>
              </div>
              <div className="indicators__content__details__third">
                {indicatorSelected.indicator ===
                  'Interesados con baja satisfacción' ||
                indicatorSelected.indicator ===
                  'Interesados con desconocimiento del objetivo del proyecto' ||
                indicatorSelected.indicator ===
                  'Promedio del conocimiento logrado con el entrenamiento' ? (
                  <div className="indicators__content__details__third__quiz">
                    <div className="indicators__content-subtitle">ENCUESTA</div>
                    <div className="indicators__content__details__third__quiz-text">
                      <Button
                        size="small"
                        variant="text"
                        disableElevation
                        className="indicators__content__details__third__quiz-button"
                        onClick={() => onClickQuiz()}
                      >
                        Link de encuesta
                      </Button>
                    </div>
                  </div>
                ) : (
                  <div className="indicators__content__details__third__period">
                    <div className="indicators__content-subtitle">PERÍODO</div>
                    <div className="indicators__content__details__third__period-text">
                      {indicatorSelected.period}
                    </div>
                  </div>
                )}

                <div className="indicators__content__details__third__goal">
                  <div className="flex">
                    <div className="indicators__content-subtitle">OBJETIVO</div>
                    <IconButton
                      aria-label="edit"
                      onClick={() => setViewModalEditGoal(true)}
                    >
                      <Edit />
                    </IconButton>
                  </div>
                  {indicatorSelected.goal ? (
                    <div className="indicators__content__details__third__goal-text">
                      Menor a {indicatorSelected.goal}%
                    </div>
                  ) : (
                    <div className="indicators__content__details__third__goal-text">
                      Objetivo no registrado
                    </div>
                  )}
                </div>
              </div>
              <div className="indicators__content__details__fourth">
                <div className="indicators__content-subtitle">RESULTADO</div>
                {indicatorSelected.result > indicatorSelected.goal ? (
                  <div className="indicators__content__details__fourth-text">
                    {indicatorSelected?.result?.toFixed(1)}%
                  </div>
                ) : (
                  <div className="indicators__content__details__fourth-text-green">
                    {indicatorSelected?.result?.toFixed(1)}%
                  </div>
                )}
              </div>
            </div>
          ) : (
            <div className="indicators__content__details">
              <div className="indicators__content__details__first">
                <div className="indicators__content-subtitle">DESCRIPCIÓN</div>
                <div className="indicators__content__details__first-text">
                  {indicators[0]?.description}
                </div>
              </div>
              <div className="indicators__content__details__second">
                <div className="indicators__content__details__second__formula">
                  <div className="flex">
                    <div className="indicators__content-subtitle">FÓRMULA</div>
                    <IconButton
                      aria-label="variables"
                      onClick={() => setViewModalSeeVariables(true)}
                    >
                      <HelpOutline className="indicators__content-subtitle-icon" />
                    </IconButton>
                  </div>
                  <div className="indicators__content__details__second__formula-text">
                    {indicators[0]?.formula}
                  </div>
                </div>
                <div className="indicators__content__details__second__measure">
                  <div className="indicators__content-subtitle">
                    UNIDAD DE MEDICIÓN
                  </div>
                  <div className="indicators__content__details__second__measure-text">
                    {indicators[0]?.measureUnit}
                  </div>
                </div>
              </div>
              <div className="indicators__content__details__third">
                <div className="indicators__content__details__third__quiz">
                  <div className="indicators__content-subtitle">ENCUESTA</div>
                  <div className="indicators__content__details__third__quiz-text">
                    <Button
                      size="small"
                      variant="text"
                      disableElevation
                      className="indicators__content__details__third__quiz-button"
                      onClick={() => onClickQuiz1()}
                    >
                      Link de encuesta
                    </Button>
                  </div>
                </div>
                <div className="indicators__content__details__third__goal">
                  <div className="flex">
                    <div className="indicators__content-subtitle">OBJETIVO</div>
                    <IconButton
                      aria-label="edit"
                      onClick={() => setViewModalEditGoal(true)}
                    >
                      <Edit />
                    </IconButton>
                  </div>
                  <div className="indicators__content__details__third__goal-text">
                    Menor a {indicators[0]?.goal}%
                  </div>
                </div>
              </div>
              <div className="indicators__content__details__fourth">
                <div className="indicators__content-subtitle">RESULTADO</div>
                <div className="indicators__content__details__fourth-text">
                  {indicators[0]?.result}
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
      <ModalEditGoal
        visible={viewModalEditGoal}
        onClose={() => setViewModalEditGoal(false)}
        selectedIndicator={indicatorSelected?.id}
      ></ModalEditGoal>
      <ModalSeeVariables
        visible={viewModalSeeVariables}
        onClose={() => setViewModalSeeVariables(false)}
        nameVar1={indicatorSelected?.nameVariable1}
        nameVar2={indicatorSelected?.nameVariable2}
        descVar1={indicatorSelected?.variableDescription1}
        descVar2={indicatorSelected?.variableDescription2}
      ></ModalSeeVariables>
    </div>
  )
}

export default Indicators
