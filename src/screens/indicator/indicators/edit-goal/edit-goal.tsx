import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Modal,
  TextField,
} from '@material-ui/core'
import React, { HTMLAttributes, useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IState } from '../../../../store/reducers'
import { indicatorsLoadAction } from '../../../../store/indicators/actions/load.action'
import { updateIndicatorAction } from '../../../../store/indicators/actions/update.actions'
import './edit-goal.scss'

export interface ModalEditGoalProps extends HTMLAttributes<HTMLDivElement> {
  visible?: boolean
  onClose?: any
  selectedIndicator?: number
}

export const ModalEditGoal = ({
  visible,
  onClose,
  selectedIndicator,
}: ModalEditGoalProps) => {
  const dispatch = useDispatch()
  const [goal, setGoal] = useState(0)
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)

  const editGoal = () => {
    dispatch(
      updateIndicatorAction.request({
        idIndicator: selectedIndicator,
        goal,
      })
    )
    dispatch(indicatorsLoadAction.request(strategicPlan?.id))
    onClose()
  }

  const handleChange = (event) => {
    setGoal(event.target.value)
  }

  return (
    <div className="edit-goal">
      <Dialog
        open={visible || false}
        onClose={onClose}
        fullWidth
        maxWidth="sm"
        PaperProps={{
          style: {
            backgroundColor: '#F3F3F3',
          },
        }}
        aria-labelledby="form-dialog-title"
        className="edit-goal__dialog"
      >
        <div className="edit-goal__header">
          <Button onClick={onClose} className="edit-goal__button-x">
            X
          </Button>
        </div>
        <div className="edit-goal__content">
          <DialogTitle
            id="form-dialog-title"
            className="edit-goal__content-title"
          >
            Editar Objetivo
          </DialogTitle>
          <DialogContent>
            <p className="edit-goal__content-field">Objetivo</p>
            <div className="edit-goal__content__div">
              <TextField
                fullWidth
                id="fullWidth"
                className="edit-goal__content__div-textfield"
                onChange={handleChange}
              />
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={onClose}
              variant="contained"
              className="edit-goal__button-cancel"
            >
              Cancelar
            </Button>
            <Button
              onClick={editGoal}
              variant="contained"
              className="edit-goal__button-accept"
            >
              Editar Objetivo
            </Button>
          </DialogActions>
        </div>
      </Dialog>
    </div>
  )
}
