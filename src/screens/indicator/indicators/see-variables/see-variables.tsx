import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Modal,
  TextField,
} from '@material-ui/core'
import React, { HTMLAttributes, useCallback, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import './see-variables.scss'

export interface ModalSeeVariablesProps extends HTMLAttributes<HTMLDivElement> {
  visible?: boolean
  onClose?: any
  nameVar1?: string
  nameVar2?: string
  descVar1?: string
  descVar2?: string
}

export const ModalSeeVariables = ({
  visible,
  onClose,
  nameVar1,
  nameVar2,
  descVar1,
  descVar2,
}: ModalSeeVariablesProps) => {
  return (
    <div className="see-variables">
      <Dialog
        open={visible || false}
        onClose={onClose}
        fullWidth
        maxWidth="sm"
        PaperProps={{
          style: {
            backgroundColor: '#F3F3F3',
          },
        }}
        aria-labelledby="form-dialog-title"
        className="see-variables__dialog"
      >
        <div className="see-variables__header">
          <Button onClick={onClose} className="see-variables__button-x">
            X
          </Button>
        </div>
        <div className="see-variables__content">
          <DialogTitle
            id="form-dialog-title"
            className="see-variables__content-title"
          >
            Variables
          </DialogTitle>
          <DialogContent>
            <p className="see-variables__content-field">
              {nameVar1}: {descVar1}
            </p>
            <p className="see-variables__content-field">
              {nameVar2}: {descVar2}
            </p>
          </DialogContent>
        </div>
      </Dialog>
    </div>
  )
}
