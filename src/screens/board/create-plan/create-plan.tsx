import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Modal,
  TextField,
} from '@material-ui/core'
import React, { HTMLAttributes, useCallback, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { CreateStrategicPlanAction } from '../../../store/strategic-plan/actions/create.action'
import './create-plan.scss'

export interface ModalCreatePlanProps extends HTMLAttributes<HTMLDivElement> {
  visible?: boolean
  onClose?: any
  idUser?: number
}

export const ModalCreatePlan = ({
  visible,
  onClose,
  idUser,
}: ModalCreatePlanProps) => {
  const dispatch = useDispatch()
  const [organizationName, setOrganizationName] = useState('')

  const handleChange = (event) => {
    setOrganizationName(event.target.value)
  }

  const onCreatePlan = () => {
    dispatch(
      CreateStrategicPlanAction.request({
        idUser,
        organizationName,
      })
    )
    onClose()
  }

  return (
    <div className="create-plan">
      <Dialog
        open={visible || false}
        onClose={onClose}
        fullWidth
        maxWidth="md"
        PaperProps={{
          style: {
            backgroundColor: '#F3F3F3',
          },
        }}
        aria-labelledby="form-dialog-title"
        className="create-plan__dialog"
      >
        <div className="create-plan__header">
          <Button onClick={onClose} className="create-plan__button-x">
            X
          </Button>
        </div>
        <div className="create-plan__content">
          <DialogTitle
            id="form-dialog-title"
            className="create-plan__content-title"
          >
            Crear Plan Estratégico
          </DialogTitle>
          <DialogContent>
            <p className="create-plan__content-field">
              Nombre de la organización
            </p>
            <div className="create-plan__content__div">
              <TextField
                fullWidth
                id="fullWidth"
                className="create-plan__content__div-textfield"
                onChange={handleChange}
              />
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={onClose}
              variant="contained"
              className="create-plan__button-cancel"
            >
              Cancelar
            </Button>
            <Button
              onClick={onCreatePlan}
              variant="contained"
              className="create-plan__button-accept"
            >
              Crear Plan Estratégico
            </Button>
          </DialogActions>
        </div>
      </Dialog>
    </div>
  )
}
