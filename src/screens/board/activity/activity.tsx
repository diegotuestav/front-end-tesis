import {
  Button,
  Dialog,
  DialogContent,
  Fab,
  Grid,
  IconButton,
  TextField,
} from '@material-ui/core'
import ReactQuill from 'react-quill'
import { useTheme } from '@mui/material/styles'
import Box from '@mui/material/Box'
import Avatar from '@mui/material/Avatar'
import OutlinedInput from '@mui/material/OutlinedInput'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import Chip from '@mui/material/Chip'
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  KeyboardTimePicker,
} from '@material-ui/pickers'
import { Edit, Group } from '@material-ui/icons'
import DateFnsUtils from '@date-io/date-fns'
import React, { HTMLAttributes, useCallback, useEffect, useState } from 'react'
import ReactHtmlParser from 'react-html-parser'
import { useDispatch, useSelector } from 'react-redux'
import { IState } from '../../../store/reducers'
import 'react-quill/dist/quill.snow.css'
import './activity.scss'
import { addActivityAction } from '../../../store/activity/actions/add.actions'
import { updateActivityAction } from '../../../store/activity/actions/update.actions'
import Logo from '../../../multimedia/Logo.svg'
import { StrategicPlanListUsersAction } from '../../../store/strategic-plan/actions/list-users.action'

export interface ModalActivityProps extends HTMLAttributes<HTMLDivElement> {
  visible?: boolean
  onClose?: any
  add?: boolean
}
export const ModalActivity = ({
  visible,
  onClose,
  add,
}: ModalActivityProps) => {
  const dispatch = useDispatch()
  const [selectedDate, setSelectedDate] = useState(new Date())
  const [isEditing, setIsEditing] = useState(false)
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [notes, setNotes] = useState('')
  const [finished, setFinished] = useState(false)
  const [usersSelected, setUsersSelected] = useState([])
  const [document, setDocument] = useState(null)
  const { activity } = useSelector((state: IState) => state.activity)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { users } = useSelector((state: IState) => state.strategicPlan)
  const theme = useTheme()
  const ITEM_HEIGHT = 48
  const ITEM_PADDING_TOP = 8
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  }

  useEffect(() => {
    dispatch(StrategicPlanListUsersAction.request(strategicPlan?.id))
  }, [dispatch, strategicPlan])

  const data = users.map((u) => ({
    key: u.id,
    value: u.names,
    photo: u.photo,
  }))

  const [personName, setPersonName] = React.useState([])

  const handleChange = (event) => {
    const {
      target: { value },
    } = event
    setPersonName(typeof value === 'string' ? value.split(',') : value)
    console.log(personName)
  }

  useEffect(() => {
    setIsEditing(add)
  }, [add])

  useEffect(() => {
    if (activity && !add) {
      setTitle(activity.title)
      setDescription(activity.description)
      setNotes(activity.notes)
      setSelectedDate(activity.endDate)
      setUsersSelected(activity.users)
      setDocument(activity.document)
      setFinished(activity.finished)
    }
  }, [activity])

  const handleDateChange = (date) => {
    setSelectedDate(date)
  }

  const handleTitleChange = (event) => {
    setTitle(event.target.value)
  }

  const onSaveActivity = () => {
    dispatch(
      addActivityAction.request({
        idMacroActivity: macroactivity?.id,
        title,
        description,
        notes,
        endDate: selectedDate,
        finished,
        users: usersSelected,
        document,
      })
    )
    setTitle('')
    setDescription('')
    setNotes('')
    setSelectedDate(new Date())
    setUsersSelected([])
    setDocument(null)
    setFinished(false)
    onClose()
  }

  const onEditActivity = () => {
    dispatch(
      updateActivityAction.request({
        idActivity: activity?.id,
        title,
        description,
        notes,
        endDate: selectedDate,
        finished,
        users: usersSelected,
        document,
      })
    )
    setIsEditing(false)
    onClose()
  }

  const onEndActivity = () => {
    dispatch(
      updateActivityAction.request({
        idActivity: activity?.id,
        title,
        description,
        notes,
        endDate: selectedDate,
        finished: true,
        users: usersSelected,
        document,
      })
    )
    setIsEditing(false)
    onClose()
  }

  const onStartActivity = () => {
    setFinished(false)
  }

  const onChangeEdit = () => {
    setIsEditing(true)
  }

  return (
    <div className="activity">
      <Dialog
        disableEnforceFocus
        open={visible || false}
        onClose={onClose}
        fullWidth
        maxWidth="lg"
        PaperProps={{
          style: {
            backgroundColor: '#F3F3F3',
          },
        }}
        aria-labelledby="form-dialog-title"
        className="activity__dialog"
      >
        <div className="activity__header">
          <Button onClick={onClose} className="activity__X-button">
            X
          </Button>
        </div>
        <div className="activity__content">
          <DialogContent>
            <div className="activity__content__left">
              <div className="activity__content__left__actions">
                <div className="activity__content__left__actions-date">
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Grid container justify="space-around">
                      <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="dd/MM/yyyy"
                        margin="normal"
                        id="date picker"
                        value={selectedDate}
                        onChange={handleDateChange}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </Grid>
                  </MuiPickersUtilsProvider>
                </div>
                <div className="activity__content__left__actions-members">
                  <FormControl sx={{ m: 1, width: 300 }}>
                    <InputLabel id="demo-multiple-chip-label">
                      Usuarios
                    </InputLabel>
                    <Select
                      labelId="demo-multiple-chip-label"
                      id="demo-multiple-chip"
                      multiple
                      value={personName}
                      onChange={handleChange}
                      input={
                        <OutlinedInput
                          id="select-multiple-chip"
                          label="Añadir Usuario"
                        />
                      }
                      renderValue={(selected) => (
                        <Box
                          sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}
                        >
                          {selected.map((value) => (
                            <Chip key={value} label={value} />
                          ))}
                        </Box>
                      )}
                      MenuProps={MenuProps}
                    >
                      {data.map((name) => (
                        <MenuItem key={name.key} value={name.value}>
                          {name.value}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </div>
              </div>
              <div className="activity__content__left__activity">
                {isEditing ? (
                  <div className="activity__content__left__activity__editing">
                    <div className="activity__content__left__activity__editing__div">
                      <TextField
                        fullWidth
                        id="fullWidth"
                        value={title}
                        className="activity__content__left__activity__editing__div-textfield"
                        onChange={handleTitleChange}
                      />
                    </div>
                    <ReactQuill
                      theme="snow"
                      value={description}
                      onChange={setDescription}
                      style={{ width: '620px', height: '110px' }}
                      className="activity__reactquill"
                    />
                  </div>
                ) : (
                  <div className="activity__content__left__activity__noediting">
                    <div className="activity__content__left__activity__noediting-editing">
                      <h1 className="activity__content__left__activity__noediting-title">
                        {title}
                      </h1>
                      <IconButton
                        aria-label="edit"
                        onClick={onChangeEdit}
                        className="activity-edit-button"
                      >
                        <Edit />
                      </IconButton>
                    </div>

                    <h3 className="activity__content__left__activity__noediting-description">
                      {ReactHtmlParser(description)}
                    </h3>
                  </div>
                )}
              </div>
              <div className="activity__content__left__attachments">
                <div className="activity__content__left__attachments-subtitle">
                  Documentos adjuntos
                </div>
                <Button
                  variant="outlined"
                  className="activity__content__left__attachments-button"
                >
                  Agregar
                </Button>
              </div>
              <div className="activity__content__left__save">
                {add ? (
                  <Button
                    variant="outlined"
                    className="activity__content__left__save-button"
                    onClick={onSaveActivity}
                  >
                    Guardar
                  </Button>
                ) : (
                  <Button
                    variant="outlined"
                    className="activity__content__left__save-button"
                    onClick={onEditActivity}
                  >
                    Guardar
                  </Button>
                )}
              </div>
            </div>
            <div className="activity__line" />
            <div className="activity__content__right">
              <div className="activity__content__left__actions-mark">
                {finished ? (
                  <Button
                    variant="outlined"
                    className="activity__content__left__actions-mark-finished-button"
                    onClick={onStartActivity}
                  >
                    Finalizado
                  </Button>
                ) : (
                  <Button
                    variant="outlined"
                    className="activity__content__left__actions-mark-button"
                    onClick={onEndActivity}
                  >
                    Marcar como finalizado
                  </Button>
                )}
              </div>
              <div className="activity__content__right__subtitle">Notas:</div>
              <div className="activity__content__right__textfield">
                {isEditing ? (
                  <ReactQuill
                    theme="snow"
                    onChange={setNotes}
                    style={{ width: '570px', height: '450px' }}
                    className="activity__reactquill"
                  />
                ) : (
                  <ReactQuill
                    theme="snow"
                    onChange={setNotes}
                    value={notes}
                    style={{ width: '570px', height: '450px' }}
                    className="activity__reactquill"
                  />
                )}
              </div>
            </div>
          </DialogContent>
        </div>
      </Dialog>
    </div>
  )
}
