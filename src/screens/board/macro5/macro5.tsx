import React, { useEffect, useState } from 'react'
import ReactQuill from 'react-quill'
import { useDispatch, useSelector } from 'react-redux'
import 'react-quill/dist/quill.snow.css'
import { Save } from '@material-ui/icons'
import './macro5.scss'
import { Button } from '@material-ui/core'
import { ModalActivity } from '../activity/activity'
import { IState } from '../../../store/reducers'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro5 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const [diagnosis, setDiagnosis] = useState('')
  const [antagonism, setAntagonism] = useState('')
  const [commitment, setCommitment] = useState('')
  const [risks, setRisks] = useState('')
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)

  useEffect(() => {
    if (macroactivity) {
      setDiagnosis(macroactivity.blankSpace1)
      setAntagonism(macroactivity.blankSpace2)
      setCommitment(macroactivity.blankSpace3)
      setRisks(macroactivity.blankSpace4)
    }
  }, [macroactivity])

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan.id,
        idMacroActivityType: 5,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const onSaveMacroActivity = () => {
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity.id,
        blankSpace1: diagnosis,
        blankSpace2: antagonism,
        blankSpace3: commitment,
        blankSpace4: risks,
        document1: null,
        document2: null,
      })
    )
  }

  const onViewActivity = () => {
    setViewModalActivity(true)
  }

  return (
    <div>
      <div className="macro5">
        <div className="macro5-content">
          <div className="macro5-title">
            Características de la cultura organizacional
          </div>
          <div className="macro5__section">
            <div className="macro5__subtitle">
              Diagnóstico de cultura organizacional y reflejo potencial en
              cambio planeado
            </div>
            <ReactQuill
              theme="snow"
              onChange={setDiagnosis}
              value={diagnosis}
              style={{ width: '1300px', height: '150px' }}
              className="macro5__reactquill"
            />
          </div>
          <div className="macro5__section">
            <div className="macro5__subtitle">
              Elementos de la cultura organizacional
            </div>
            <div className="macro5__double">
              <div className="macro5__section-sub">
                <div className="macro5__subtitle-sub">
                  Factores de antagonismo
                </div>
                <ReactQuill
                  theme="snow"
                  onChange={setAntagonism}
                  value={antagonism}
                  style={{ width: '650px', height: '150px' }}
                  className="macro5__reactquill"
                />
              </div>
              <div className="macro5__section-sub">
                <div className="macro5__subtitle-sub">
                  Factores de compromiso
                </div>
                <ReactQuill
                  theme="snow"
                  onChange={setCommitment}
                  value={commitment}
                  style={{ width: '650px', height: '150px' }}
                  className="macro5__reactquill"
                />
              </div>
            </div>
          </div>
          <div className="macro5__section">
            <div className="macro5__subtitle">
              Riesgos inherentes al factor humano
            </div>
            <ReactQuill
              theme="snow"
              onChange={setRisks}
              value={risks}
              style={{ width: '1300px', height: '150px' }}
              className="macro5__reactquill"
            />
          </div>
          <div className="macro5__activities">
            <div className="macro5__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro5__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro5__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>

        <div className="macro5__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro5__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}

export default Macro5
