import { Button } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import ReactQuill from 'react-quill'
import { useDispatch, useSelector } from 'react-redux'
import 'react-quill/dist/quill.snow.css'
import './macro10.scss'
import { Save } from '@material-ui/icons'
import { ModalActivity } from '../activity/activity'
import { IState } from '../../../store/reducers'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro10 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const [plan, setPlan] = useState('')
  const [strategy, setStrategy] = useState('')
  const [approach, setApproach] = useState('')
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)

  useEffect(() => {
    if (macroactivity) {
      setPlan(macroactivity.blankSpace1)
      setStrategy(macroactivity.blankSpace2)
      setApproach(macroactivity.blankSpace3)
    }
  }, [macroactivity])

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan.id,
        idMacroActivityType: 10,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const onSaveMacroActivity = () => {
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity.id,
        blankSpace1: plan,
        blankSpace2: strategy,
        blankSpace3: approach,
        blankSpace4: '',
        document1: null,
        document2: null,
      })
    )
  }

  const onViewActivity = () => {
    setViewModalActivity(true)
  }
  return (
    <div>
      <div className="macro10">
        <div className="macro10-content">
          <div className="macro10-title">
            Plan de Acción de Gestión del Cambio
          </div>
          <div className="macro10__section">
            <div className="macro10__subtitle">Plan de Acción</div>
            <ReactQuill
              theme="snow"
              onChange={setPlan}
              value={plan}
              style={{ width: '1300px', height: '150px' }}
              className="macro10__reactquill"
            />
          </div>
          <div className="macro10__section">
            <div className="macro10__subtitle">
              Estrategia de sostenimiento de cambios
            </div>
            <ReactQuill
              theme="snow"
              onChange={setStrategy}
              value={strategy}
              style={{ width: '1300px', height: '150px' }}
              className="macro10__reactquill"
            />
          </div>
          <div className="macro10__section">
            <div className="macro10__subtitle">
              Abordaje de Gestión del Cambio
            </div>
            <ReactQuill
              theme="snow"
              onChange={setApproach}
              value={approach}
              style={{ width: '1300px', height: '150px' }}
              className="macro10__reactquill"
            />
          </div>
          <div className="macro10__activities">
            <div className="macro10__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro10__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro10__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>
        <div className="macro10__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro10__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}

export default Macro10
