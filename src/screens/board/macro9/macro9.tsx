import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import 'react-quill/dist/quill.snow.css'
import './macro9.scss'
import { Button } from '@material-ui/core'
import { Save } from '@material-ui/icons'
import ReactQuill from 'react-quill'
import { ModalActivity } from '../activity/activity'
import { IState } from '../../../store/reducers'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro9 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const [maturity, setMaturity] = useState('')
  const [trust, setTrust] = useState('')
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)

  useEffect(() => {
    if (macroactivity) {
      setMaturity(macroactivity.blankSpace1)
      setTrust(macroactivity.blankSpace2)
    }
  }, [macroactivity])

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan.id,
        idMacroActivityType: 9,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const onSaveMacroActivity = () => {
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity?.id,
        blankSpace1: maturity,
        blankSpace2: trust,
        blankSpace3: '',
        blankSpace4: '',
        document1: null,
        document2: null,
      })
    )
  }

  const onViewActivity = () => {
    setViewModalActivity(true)
  }

  return (
    <div>
      <div className="macro9">
        <div className="macro9-content">
          <div className="macro9-title">
            Predisposición del clima para cambios
          </div>
          <div className="macro9__section">
            <div className="macro9__subtitle">
              Evaluación del nivel de madurez
            </div>
            <ReactQuill
              theme="snow"
              onChange={setMaturity}
              value={maturity}
              style={{ width: '1300px', height: '150px' }}
              className="macro9__reactquill"
            />
          </div>
          <div className="macro9__section">
            <div className="macro9__subtitle">
              Evaluación del nivel de confianza
            </div>
            <ReactQuill
              theme="snow"
              onChange={setTrust}
              value={trust}
              style={{ width: '1300px', height: '150px' }}
              className="macro9__reactquill"
            />
          </div>

          <div className="macro9__activities">
            <div className="macro9__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro9__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro9__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>
        <div className="macro9__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro9__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}

export default Macro9
