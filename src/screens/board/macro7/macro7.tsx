import React, { useEffect, useState } from 'react'
import ReactQuill from 'react-quill'
import { useDispatch, useSelector } from 'react-redux'
import 'react-quill/dist/quill.snow.css'
import './macro7.scss'
import { Button } from '@material-ui/core'
import { Save } from '@material-ui/icons'
import { ModalActivity } from '../activity/activity'
import { IState } from '../../../store/reducers'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro7 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const [necesities, setNecesities] = useState('')
  const [enviroment, setEnviroment] = useState('')
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)

  useEffect(() => {
    if (macroactivity) {
      setNecesities(macroactivity.blankSpace1)
      setEnviroment(macroactivity.blankSpace2)
    }
  }, [macroactivity])

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan.id,
        idMacroActivityType: 7,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const onSaveMacroActivity = () => {
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity.id,
        blankSpace1: necesities,
        blankSpace2: enviroment,
        blankSpace3: '',
        blankSpace4: '',
        document1: null,
        document2: null,
      })
    )
  }

  const onViewActivity = () => {
    setViewModalActivity(true)
  }

  return (
    <div>
      <div className="macro7">
        <div className="macro7-content">
          <div className="macro7-title">
            Ambiente físico adecuado al proyecto
          </div>
          <div className="macro7__section">
            <div className="macro7__subtitle">
              Necesidades, inversiones y beneficios del ambiente fisico
            </div>
            <ReactQuill
              theme="snow"
              onChange={setNecesities}
              value={necesities}
              style={{ width: '1300px', height: '150px' }}
              className="macro7__reactquill"
            />
          </div>
          <div className="macro7__section">
            <div className="macro7__subtitle">
              Efectos del ambiente físico en el equipo
            </div>
            <ReactQuill
              theme="snow"
              onChange={setEnviroment}
              value={enviroment}
              style={{ width: '1300px', height: '150px' }}
              className="macro7__reactquill"
            />
          </div>

          <div className="macro7__activities">
            <div className="macro7__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro7__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro7__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>

        <div className="macro7__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro7__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}

export default Macro7
