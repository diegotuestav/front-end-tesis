import React, { useEffect, useState } from 'react'
import ReactQuill from 'react-quill'
import { useDispatch, useSelector } from 'react-redux'
import 'react-quill/dist/quill.snow.css'
import './macro3.scss'
import { Save } from '@material-ui/icons'
import { Button } from '@material-ui/core'
import { IState } from '../../../store/reducers'
import { ModalActivity } from '../activity/activity'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro3 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)
  const [vision, setVision] = useState('')
  const [purpose, setPurpose] = useState('')
  const [identity, setIdentity] = useState('')

  useEffect(() => {
    if (macroactivity) {
      setVision(macroactivity.blankSpace1)
      setPurpose(macroactivity.blankSpace2)
      setIdentity(macroactivity.blankSpace3)
    }
  }, [macroactivity])

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan.id,
        idMacroActivityType: 3,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const onSaveMacroActivity = () => {
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity.id,
        blankSpace1: vision,
        blankSpace2: purpose,
        blankSpace3: identity,
        blankSpace4: '',
        document1: null,
        document2: null,
      })
    )
  }

  const onViewActivity = () => {
    setViewModalActivity(true)
  }

  return (
    <div>
      <div className="macro3">
        <div className="macro3-content">
          <div className="macro3-title">Propósito e identidad del proyecto</div>
          <div className="macro3__section">
            <div className="macro3__subtitle">Visión</div>
            <ReactQuill
              theme="snow"
              onChange={setVision}
              value={vision}
              style={{ width: '1300px', height: '150px' }}
              className="macro3__reactquill"
            />
          </div>
          <div className="macro3__section">
            <div className="macro3__subtitle">Propósito del cambio</div>
            <ReactQuill
              theme="snow"
              onChange={setPurpose}
              value={purpose}
              style={{ width: '1300px', height: '150px' }}
              className="macro3__reactquill"
            />
          </div>
          <div className="macro3__section">
            <div className="macro3__subtitle">Identidad del proyecto</div>
            <ReactQuill
              theme="snow"
              onChange={setIdentity}
              value={identity}
              style={{ width: '1300px', height: '150px' }}
              className="macro3__reactquill"
            />
          </div>
          <div className="macro3__activities">
            <div className="macro3__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro3__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro3__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>
        <div className="macro3__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro3__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}

export default Macro3
