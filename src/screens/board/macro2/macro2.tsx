import { Button } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import ReactQuill from 'react-quill'
import { Save } from '@material-ui/icons'
import { useDispatch, useSelector } from 'react-redux'
import { IState } from '../../../store/reducers'
import 'react-quill/dist/quill.snow.css'
import './macro2.scss'
import { ModalActivity } from '../activity/activity'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro2 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)
  const [strategy, setStrategy] = useState('')
  const [metrics, setMetrics] = useState('')

  useEffect(() => {
    if (macroactivity) {
      setStrategy(macroactivity.blankSpace1)
      setMetrics(macroactivity.blankSpace2)
    }
  }, [macroactivity])

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan?.id,
        idMacroActivityType: 2,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const onSaveMacroActivity = () => {
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity.id,
        blankSpace1: strategy,
        blankSpace2: metrics,
        blankSpace3: '',
        blankSpace4: '',
        document1: null,
        document2: null,
      })
    )
  }

  const onViewActivity = () => {
    setViewModalActivity(true)
  }

  return (
    <div>
      <div className="macro2">
        <div className="macro2-content">
          <div className="macro2-title">
            Workshop de alineación y movilización de líderes
          </div>
          <div className="macro2__section">
            <div className="macro2__subtitle">
              Estrategia de comunicación del cambio
            </div>
            <ReactQuill
              theme="snow"
              onChange={setStrategy}
              value={strategy}
              style={{ width: '1300px', height: '250px' }}
              className="macro2__reactquill"
            />
          </div>
          <div className="macro2__section">
            <div className="macro2__subtitle">
              Metas y métricas cuantitativas y cualitativas
            </div>
            <ReactQuill
              theme="snow"
              onChange={setMetrics}
              value={metrics}
              style={{ width: '1300px', height: '250px' }}
              className="macro2__reactquill"
            />
          </div>
          <div className="macro2__activities">
            <div className="macro2__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro2__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro2__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>
        <div className="macro2__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro2__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}

export default Macro2
