import { Button } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import ReactQuill from 'react-quill'
import { useDispatch, useSelector } from 'react-redux'
import { Save } from '@material-ui/icons'
import 'react-quill/dist/quill.snow.css'
import './macro11.scss'
import { ModalActivity } from '../activity/activity'
import { IState } from '../../../store/reducers'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro11 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const [schedule, setSchedule] = useState('')
  const [goals, setGoals] = useState('')
  const [investments, setInvestments] = useState('')
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)

  useEffect(() => {
    if (macroactivity) {
      setSchedule(macroactivity.blankSpace1)
      setGoals(macroactivity.blankSpace2)
      setInvestments(macroactivity.blankSpace3)
    }
  }, [macroactivity])

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan.id,
        idMacroActivityType: 11,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const onSaveMacroActivity = () => {
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity.id,
        blankSpace1: schedule,
        blankSpace2: goals,
        blankSpace3: investments,
        blankSpace4: '',
        document1: null,
        document2: null,
      })
    )
  }
  const onViewActivity = () => {
    setViewModalActivity(true)
  }
  return (
    <div>
      <div className="macro11">
        <div className="macro11-content">
          <div className="macro11-title">Kick-off del proyecto</div>

          <div className="macro11__section">
            <div className="macro11__subtitle">Agenda</div>
            <ReactQuill
              theme="snow"
              onChange={setSchedule}
              value={schedule}
              style={{ width: '1300px', height: '150px' }}
              className="macro11__reactquill"
            />
          </div>
          <div className="macro11__section">
            <div className="macro11__subtitle">Objetivos</div>
            <ReactQuill
              theme="snow"
              onChange={setGoals}
              value={goals}
              style={{ width: '1300px', height: '150px' }}
              className="macro11__reactquill"
            />
          </div>
          <div className="macro11__section">
            <div className="macro11__subtitle">Inversiones previstas</div>
            <ReactQuill
              theme="snow"
              onChange={setInvestments}
              value={investments}
              style={{ width: '1300px', height: '150px' }}
              className="macro11__reactquill"
            />
          </div>
          <div className="macro11__activities">
            <div className="macro11__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro11__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro11__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>
        <div className="macro11__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro11__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}
export default Macro11
