import { Button, createStyles, makeStyles } from '@material-ui/core'
import { Save } from '@material-ui/icons'
import { DropzoneArea } from 'material-ui-dropzone'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IState } from '../../../store/reducers'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { ModalActivity } from '../activity/activity'
import './macro6.scss'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro6 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const [uploadedFileRACI, setUploadedFileRACI] = useState(null)
  const [uploadedFileOrganigrama, setUploadedFileOrganigrama] = useState<Blob>(
    null
  )
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)

  const useStylesChips = makeStyles((theme) =>
    createStyles({
      previewChip: {
        minWidth: 160,
        maxWidth: 210,
      },
    })
  )
  useEffect(() => {
    if (macroactivity) {
      setUploadedFileRACI(macroactivity.document1)
      setUploadedFileOrganigrama(macroactivity.document2)
    }
  }, [macroactivity])

  const classes = useStylesChips()

  const handleOnChangeFileRACI = (file) => {
    setUploadedFileRACI(file)
  }
  const handleOnChangeFileOrganigrama = (file) => {
    setUploadedFileOrganigrama(file)
  }

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan?.id,
        idMacroActivityType: 6,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const onSaveMacroActivity = () => {
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity?.id,
        blankSpace1: '',
        blankSpace2: '',
        blankSpace3: '',
        blankSpace4: '',
        document1: uploadedFileRACI,
        document2: uploadedFileOrganigrama,
      })
    )
  }

  const onViewActivity = () => {
    setViewModalActivity(true)
  }

  return (
    <div>
      <div className="macro6">
        <div className="macro6__content">
          <div className="macro6-title">Roles y responsabilidades</div>

          <div className="macro6__section">
            <div className="macro6__subtitle">Matriz RACI</div>
            <DropzoneArea
              dropzoneText="Subir documento"
              acceptedFiles={['.pdf']}
              showPreviews
              showPreviewsInDropzone={false}
              useChipsForPreview
              previewGridProps={{ container: { spacing: 1, direction: 'row' } }}
              previewChipProps={{ classes: { root: classes.previewChip } }}
              previewText="Archivo seleccionado:"
              filesLimit={1}
              onChange={(files) => handleOnChangeFileRACI(files)}
            />
          </div>
          <div className="macro6__section">
            <div className="macro6__subtitle">Organigrama</div>
            <DropzoneArea
              dropzoneText="Subir documento"
              acceptedFiles={['.pdf']}
              showPreviews
              showPreviewsInDropzone={false}
              useChipsForPreview
              previewGridProps={{ container: { spacing: 1, direction: 'row' } }}
              previewChipProps={{ classes: { root: classes.previewChip } }}
              previewText="Archivo seleccionado:"
              filesLimit={1}
              onChange={(files) => handleOnChangeFileOrganigrama(files)}
            />
          </div>
          <div className="macro6__activities">
            <div className="macro6__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro6__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro6__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>
        <div className="macro6__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro6__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}

export default Macro6
