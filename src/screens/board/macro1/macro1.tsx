import { Button, Checkbox, TextField } from '@material-ui/core'
import ReactQuill from 'react-quill'
import { Add, Check, Save } from '@material-ui/icons'
import MaterialTable from 'material-table'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IState } from '../../../store/reducers'
import 'react-quill/dist/quill.snow.css'
import './macro1.scss'
import { ModalActivity } from '../activity/activity'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { sponsorshipLoadAction } from '../../../store/sponsorship/actions/load.action'
import { addSponsorshipAction } from '../../../store/sponsorship/actions/add.actions'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { updateSponsorshipAction } from '../../../store/sponsorship/actions/update.actions'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro1 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)

  const [sponsorName, setSponsorName] = useState('')
  const [sponsorLastName, setSponsorLastName] = useState('')
  const [sponsorCharge, setSponsorCharge] = useState('')
  const [sponsorEmail, setSponsorEmail] = useState('')
  const [sponsorPhone, setSponsorPhone] = useState('')

  const [comiteName, setComiteName] = useState('')
  const [comiteLastName, setComiteLastName] = useState('')
  const [comiteCharge, setComiteCharge] = useState('')
  const [comiteEmail, setComiteEmail] = useState('')
  const [comitePhone, setComitePhone] = useState('')

  const [isComite, setIsComite] = useState(false)
  const [impacts, setImpacts] = useState('')

  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)
  const { sponsorship } = useSelector((state: IState) => state.sponsorship)

  useEffect(() => {
    if (macroactivity) {
      setImpacts(macroactivity?.blankSpace1)
    }
  }, [macroactivity])

  useEffect(() => {
    if (sponsorship.length > 1) setIsComite(true)
    if (sponsorship) {
      const sponsor = sponsorship.filter((s) => s.role === 'Patrocinador')
      setSponsorName(sponsor[0]?.names)
      setSponsorLastName(sponsor[0]?.lastnames)
      setSponsorCharge(sponsor[0]?.charge)
      setSponsorEmail(sponsor[0]?.email)
      setSponsorPhone(sponsor[0]?.phone)

      const comiteSponsor = sponsorship.filter(
        (s) => s.role === 'Coordinador de cómite de patrocinio'
      )
      setComiteName(comiteSponsor[0]?.names)
      setComiteLastName(comiteSponsor[0]?.lastnames)
      setComiteCharge(comiteSponsor[0]?.charge)
      setComiteEmail(comiteSponsor[0]?.email)
      setComitePhone(comiteSponsor[0]?.phone)
    }
  }, [sponsorship])

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan?.id,
        idMacroActivityType: 1,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  useEffect(() => {
    dispatch(sponsorshipLoadAction.request(strategicPlan?.id))
  }, [dispatch, sponsorshipLoadAction, strategicPlan])

  const columnas = [
    {
      title: 'Nombre(s)',
      field: 'names',
    },
    {
      title: 'Apellido(s)',
      field: 'lastnames',
    },
    {
      title: 'Correo Electrónico',
      field: 'email',
    },
    {
      title: 'Teléfono',
      field: 'phone',
    },
    {
      title: 'Cargo',
      field: 'charge',
    },
  ]

  const data = sponsorship
    .filter((u) => u.role === 'Miembro de cómite de patrocinio')
    .map((u) => ({
      names: u.names,
      lastnames: u.lastnames,
      email: u.email,
      phone: u.phone,
      charge: u.charge,
    }))

  const handleChangeSponsorName = (event) => {
    setSponsorName(event.target.value)
  }
  const handleChangeSponsorLastName = (event) => {
    setSponsorLastName(event.target.value)
  }

  const handleChangeSponsorCharge = (event) => {
    setSponsorCharge(event.target.value)
  }

  const handleChangeSponsorEmail = (event) => {
    setSponsorEmail(event.target.value)
  }
  const handleChangeSponsorPhone = (event) => {
    setSponsorPhone(event.target.value)
  }
  const handleChangeComiteName = (event) => {
    setComiteName(event.target.value)
  }
  const handleChangeComiteLastName = (event) => {
    setComiteLastName(event.target.value)
  }

  const handleChangeComiteCharge = (event) => {
    setComiteCharge(event.target.value)
  }

  const handleChangeComiteEmail = (event) => {
    setComiteEmail(event.target.value)
  }
  const handleChangeComitePhone = (event) => {
    setComitePhone(event.target.value)
  }
  const handleChangeComite = (event) => {
    setIsComite(event.target.checked)
  }

  const onSaveMacroActivity = () => {
    const sponsor = sponsorship.filter((s) => s.role === 'Patrocinador')
    const comite = sponsorship.filter(
      (s) => s.role === 'Coordinador de cómite de patrocinio'
    )
    if (sponsor.length === 0) {
      dispatch(
        addSponsorshipAction.request({
          names: sponsorName,
          lastnames: sponsorLastName,
          email: sponsorEmail,
          phone: sponsorPhone,
          charge: sponsorCharge,
          idRole: 1,
          idStrategicPlan: strategicPlan?.id,
        })
      )
    } else {
      dispatch(
        updateSponsorshipAction.request({
          idSponsorship: sponsor[0].id,
          names: sponsorName,
          lastnames: sponsorLastName,
          email: sponsorEmail,
          phone: sponsorPhone,
          charge: sponsorCharge,
          idRole: 1,
          idStrategicPlan: strategicPlan?.id,
        })
      )
    }

    if (isComite) {
      if (comite.length === 0) {
        dispatch(
          addSponsorshipAction.request({
            names: comiteName,
            lastnames: comiteLastName,
            email: comiteEmail,
            phone: comitePhone,
            charge: comiteCharge,
            idRole: 2,
            idStrategicPlan: strategicPlan?.id,
          })
        )
      } else {
        dispatch(
          updateSponsorshipAction.request({
            idSponsorship: comite[0]?.id,
            names: comiteName,
            lastnames: comiteLastName,
            email: comiteEmail,
            phone: comitePhone,
            charge: comiteCharge,
            idRole: 2,
            idStrategicPlan: strategicPlan?.id,
          })
        )
      }
    }
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity?.id,
        blankSpace1: impacts,
        blankSpace2: '',
        blankSpace3: '',
        blankSpace4: '',
        document1: null,
        document2: null,
      })
    )
  }

  const onViewActivity = () => {
    setViewModalActivity(true)
  }

  return (
    <div>
      <div className="macro1">
        <div className="macro1-content">
          <div className="macro1-title">Patrocinador del Proyecto</div>
          <div className="macro1__sponsor">
            <div className="macro1__sponsor-first">
              <h1 className="macro1__sponsor-first-name">Nombres:</h1>
              <div className="macro1__members__div">
                <TextField
                  fullWidth
                  id="fullWidth"
                  value={sponsorName}
                  className="macro1__members__div-textfield"
                  onChange={handleChangeSponsorName}
                />
              </div>
              <h1 className="macro1__sponsor-first-lastnames">Apellidos:</h1>
              <div className="macro1__members__div">
                <TextField
                  fullWidth
                  id="fullWidth"
                  value={sponsorLastName}
                  className="macro1__members__div-textfield"
                  onChange={handleChangeSponsorLastName}
                />
              </div>
              <h1 className="macro1__sponsor-first-charge">Cargo:</h1>
              <div className="macro1__members__div">
                <TextField
                  fullWidth
                  id="fullWidth"
                  value={sponsorCharge}
                  className="macro1__members__div-textfield"
                  onChange={handleChangeSponsorCharge}
                />
              </div>
            </div>
            <div className="macro1__sponsor-second">
              <h1 className="macro1__sponsor-second-email">
                Correo Electrónico:
              </h1>
              <div className="macro1__members__div">
                <TextField
                  fullWidth
                  id="fullWidth"
                  value={sponsorEmail}
                  className="macro1__members__div-textfield"
                  onChange={handleChangeSponsorEmail}
                />
              </div>
              <h1 className="macro1__sponsor-second-phone">Teléfono:</h1>
              <div className="macro1__members__div">
                <TextField
                  fullWidth
                  id="fullWidth"
                  value={sponsorPhone}
                  className="macro1__members__div-textfield"
                  onChange={handleChangeSponsorPhone}
                />
              </div>
            </div>
          </div>
          <div className="macro1__is-comite">
            <div className="macro1__subtitle">
              Coordinador de cómite de patrocinio
            </div>
            <Checkbox
              checked={isComite}
              onClick={handleChangeComite}
              size="small"
            />
          </div>
          {isComite ? (
            <div className="macro1__comite">
              <div className="macro1__comite__coord">
                <div className="macro1__sponsor-first">
                  <h1 className="macro1__sponsor-first-name">Nombres:</h1>
                  <div className="macro1__members__div">
                    <TextField
                      fullWidth
                      value={comiteName}
                      id="fullWidth"
                      className="macro1__members__div-textfield"
                      onChange={handleChangeComiteName}
                    />
                  </div>
                  <h1 className="macro1__sponsor-first-lastnames">
                    Apellidos:
                  </h1>
                  <div className="macro1__members__div">
                    <TextField
                      fullWidth
                      value={comiteLastName}
                      id="fullWidth"
                      className="macro1__members__div-textfield"
                      onChange={handleChangeComiteLastName}
                    />
                  </div>
                  <h1 className="macro1__sponsor-first-charge">Cargo:</h1>
                  <div className="macro1__members__div">
                    <TextField
                      fullWidth
                      value={comiteCharge}
                      id="fullWidth"
                      className="macro1__members__div-textfield"
                      onChange={handleChangeComiteCharge}
                    />
                  </div>
                </div>
                <div className="macro1__sponsor-second">
                  <h1 className="macro1__sponsor-second-email">
                    Correo Electrónico:
                  </h1>
                  <div className="macro1__members__div">
                    <TextField
                      fullWidth
                      value={comiteEmail}
                      id="fullWidth"
                      className="macro1__members__div-textfield"
                      onChange={handleChangeComiteEmail}
                    />
                  </div>
                  <h1 className="macro1__sponsor-second-phone">Teléfono:</h1>
                  <div className="macro1__members__div">
                    <TextField
                      fullWidth
                      value={comitePhone}
                      id="fullWidth"
                      className="macro1__members__div-textfield"
                      onChange={handleChangeComitePhone}
                    />
                  </div>
                </div>
              </div>
              <div className="macro1__comite__members">
                <div className="macro1__subtitle">Cómite de patrocinio</div>
                <Button
                  variant="contained"
                  className="macro1__comite-button"
                  startIcon={<Add />}
                >
                  Añadir Integrante
                </Button>

                <div className="macro1__comite__members__table">
                  <MaterialTable
                    columns={columnas}
                    title=""
                    data={data}
                    options={{
                      actionsColumnIndex: -1,
                    }}
                    localization={{
                      header: {
                        actions: 'Acciones',
                      },
                    }}
                  />
                </div>
              </div>
            </div>
          ) : (
            <></>
          )}
          <div className="macro1__impacts">
            <div className="macro1__subtitle">
              Impactos organizacionales preliminares
            </div>
            <ReactQuill
              theme="snow"
              onChange={setImpacts}
              value={impacts}
              style={{ width: '1300px', height: '250px' }}
              className="macro1__reactquill"
            />
          </div>
          <div className="macro1__activities">
            <div className="macro1__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro1__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro1__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>
        <div className="macro1__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro1__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}

export default Macro1
