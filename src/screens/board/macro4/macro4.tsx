import { Button } from '@material-ui/core'
import { ImportContacts, PersonAdd } from '@material-ui/icons'
import MaterialTable from 'material-table'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IState } from '../../../store/reducers'
import { ModalActivity } from '../activity/activity'
import 'react-quill/dist/quill.snow.css'
import './macro4.scss'
import { stakeholdersLoadAction } from '../../../store/stakeholders/actions/load.action'
import { collectivesLoadAction } from '../../../store/stakeholders/actions/list-collectives.action'
import { ModalAddStakeholder } from './add-stakeholder/add-stakeholder'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro4 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const [viewModalAddStakeholder, setViewModalAddStakeholder] = useState(false)
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)
  const { stakeholders } = useSelector((state: IState) => state.stakeholder)
  const { collectives } = useSelector((state: IState) => state.stakeholder)

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan.id,
        idMacroActivityType: 4,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const columnas = [
    {
      title: 'Nombre(s)',
      field: 'names',
    },
    {
      title: 'Apellido(s)',
      field: 'lastnames',
    },
    {
      title: 'Perfil',
      field: 'profile',
    },
    {
      title: 'Adhesión al cambio',
      field: 'adhesion',
    },
    {
      title: 'Grupo',
      field: 'group',
    },
  ]

  useEffect(() => {
    dispatch(stakeholdersLoadAction.request(strategicPlan?.id))
  }, [dispatch, strategicPlan])

  useEffect(() => {
    dispatch(collectivesLoadAction.request(strategicPlan?.id))
  }, [dispatch, strategicPlan])

  const data = stakeholders.map((u) => ({
    names: u.names,
    lastnames: u.lastnames,
    profile: u.profile,
    adhesion: u.adhesion,
    group: u.collective,
  }))

  const onViewActivity = () => {
    setViewModalActivity(true)
  }

  return (
    <div>
      <div className="macro4">
        <div className="macro4-title">Clasificación de los interesados</div>
        <div className="macro4__actions">
          <Button
            variant="contained"
            className="macro4__actions-button1"
            startIcon={<PersonAdd />}
            onClick={() => setViewModalAddStakeholder(true)}
          >
            Añadir Integrante
          </Button>
        </div>
        <div className="team__table">
          <MaterialTable
            columns={columnas}
            title=""
            data={data}
            actions={[
              {
                icon: 'delete',
                tooltip: 'Eliminar Vehículo',
                onClick: (event, rowData) =>
                  // eslint-disable-next-line
                  window.confirm(
                    // eslint-disable-next-line
                    'Estas seguro que deseas eliminar el vehiculo: '
                  ),
              },
            ]}
            options={{
              actionsColumnIndex: -1,
            }}
            localization={{
              header: {
                actions: 'Acciones',
              },
            }}
          />
        </div>
        <div className="macro4__activities">
          <div className="macro4__subtitle">Actividades</div>
          <Button
            variant="outlined"
            className="macro4__activities-button"
            onClick={() => setViewAddModalActivity(true)}
          >
            Agregar
          </Button>
        </div>
        <div className="macro4__activities__list">
          {macroactivity?.activities.map((a, index) => (
            <ActivityCard
              activity={a}
              isDone={a.finished}
              onClickView={onViewActivity}
            />
          ))}
        </div>
      </div>
      <ModalAddStakeholder
        visible={viewModalAddStakeholder}
        onClose={() => setViewModalAddStakeholder(false)}
        collectives={collectives}
      ></ModalAddStakeholder>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}

export default Macro4
