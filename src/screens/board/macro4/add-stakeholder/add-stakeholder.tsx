import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputLabel,
  Modal,
  NativeSelect,
  TextField,
} from '@material-ui/core'
import { TextFieldsOutlined } from '@material-ui/icons'
import React, { HTMLAttributes, useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IState } from '../../../../store/reducers'
import { ICollective } from '../../../../interfaces/collective.interface'
import { addStakeholderAction } from '../../../../store/stakeholders/actions/add.action'
import './add-stakeholder.scss'

export interface ModalAddStakeholderProps
  extends HTMLAttributes<HTMLDivElement> {
  visible?: boolean
  onClose?: any
  collectives?: ICollective[]
}

export const ModalAddStakeholder = ({
  visible,
  onClose,
  collectives,
}: ModalAddStakeholderProps) => {
  const dispatch = useDispatch()
  const [names, setNames] = useState('')
  const [lastnames, setLastnames] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [idProfile, setIdProfile] = useState(1)
  const [idAdhesion, setIdAdhesion] = useState(1)
  const [idCollective, setIdCollective] = useState(1)
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)

  const profile = [
    { id: 1, profile: 'Decisor' },
    { id: 2, profile: 'Influenciador Directo' },
    { id: 3, profile: 'Influenciador Indirecto' },
    { id: 4, profile: 'Espectador' },
  ]

  const adhesion = [
    { id: 1, adhesion: 'Vendedor' },
    { id: 2, adhesion: 'Soporte' },
    { id: 3, adhesion: 'Inestable' },
    { id: 4, adhesion: 'Probable Resistente' },
    { id: 5, adhesion: 'Saboteador Abierto' },
    { id: 6, adhesion: 'Saboteador Oculto' },
  ]

  const handleChangeName = (event) => {
    setNames(event.target.value)
  }

  const handleChangeLastNames = (event) => {
    setLastnames(event.target.value)
  }
  const handleChangeEmail = (event) => {
    setEmail(event.target.value)
  }
  const handleChangePhone = (event) => {
    setPhone(event.target.value)
  }
  const handleChangeProfile = (event) => {
    setIdProfile(event.target.value)
  }
  const handleChangeAdhesion = (event) => {
    setIdAdhesion(event.target.value)
  }
  const handleChangeCollective = (event) => {
    setIdCollective(event.target.value)
  }

  const addStakeholder = () => {
    dispatch(
      addStakeholderAction.request({
        names,
        lastnames,
        email,
        phone,
        idProfile,
        idAdhesion,
        idCollective,
        idStrategicPlan: strategicPlan.id,
      })
    )
    onClose()
  }
  return (
    <div className="add-stakeholder">
      <Dialog
        open={visible || false}
        onClose={onClose}
        fullWidth
        maxWidth="md"
        PaperProps={{
          style: {
            backgroundColor: '#F3F3F3',
          },
        }}
        aria-labelledby="form-dialog-title"
        className="add-stakeholder__dialog"
      >
        <div className="add-stakeholder__header">
          <Button onClick={onClose} className="add-stakeholder__button-x">
            X
          </Button>
        </div>
        <div className="add-stakeholder__content">
          <DialogTitle
            id="form-dialog-title"
            className="add-stakeholder__content-title"
          >
            Añadir Interesado
          </DialogTitle>
          <DialogContent>
            <div className="add-stakeholder__content-content">
              <div className="add-stakeholder__content-textfield">
                <TextField
                  id="names"
                  label="Nombres"
                  onChange={handleChangeName}
                  className="add-stakeholder__content-textfield-field"
                />
                <TextField
                  id="lastnames"
                  label="Apellidos"
                  onChange={handleChangeLastNames}
                  className="add-stakeholder__content-textfield-field"
                />
              </div>
              <div className="add-stakeholder__content-textfield">
                <TextField
                  id="email"
                  label="Correo Electrónico"
                  onChange={handleChangeEmail}
                  className="add-stakeholder__content-textfield-field"
                />
                <TextField
                  id="phone"
                  label="Teléfono"
                  onChange={handleChangePhone}
                  className="add-stakeholder__content-textfield-field"
                />
              </div>
              <div className="add-stakeholder__content-textfield">
                <FormControl
                  fullWidth
                  className="add-stakeholder__content-textfield-select"
                >
                  <InputLabel variant="standard" htmlFor="uncontrolled-native">
                    Perfil
                  </InputLabel>
                  <NativeSelect
                    defaultValue={1}
                    onChange={handleChangeProfile}
                    inputProps={{
                      name: 'profile',
                      id: 'uncontrolled-native',
                    }}
                  >
                    {profile.map((c, index) => (
                      <option value={c?.id}>{c?.profile}</option>
                    ))}
                  </NativeSelect>
                </FormControl>
                <FormControl
                  fullWidth
                  className="add-stakeholder__content-textfield-select"
                >
                  <InputLabel variant="standard" htmlFor="uncontrolled-native">
                    Adhesión al cambio
                  </InputLabel>
                  <NativeSelect
                    defaultValue={1}
                    onChange={handleChangeAdhesion}
                    inputProps={{
                      name: 'adhesion',
                      id: 'uncontrolled-native',
                    }}
                  >
                    {adhesion.map((c, index) => (
                      <option value={c?.id}>{c?.adhesion}</option>
                    ))}
                  </NativeSelect>
                </FormControl>
                <FormControl
                  fullWidth
                  className="add-stakeholder__content-textfield-select"
                >
                  <InputLabel variant="standard" htmlFor="uncontrolled-native">
                    Grupo
                  </InputLabel>
                  <NativeSelect
                    defaultValue={collectives[0]?.id}
                    onChange={handleChangeCollective}
                    inputProps={{
                      name: 'collective',
                      id: 'uncontrolled-native',
                    }}
                  >
                    {collectives.map((c, index) => (
                      <option value={c?.id}>{c?.collective}</option>
                    ))}
                  </NativeSelect>
                </FormControl>
              </div>
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={onClose}
              variant="contained"
              className="add-stakeholder__button-cancel"
            >
              Cancelar
            </Button>
            <Button
              onClick={addStakeholder}
              variant="contained"
              className="add-stakeholder__button-accept"
            >
              Añadir Interesado
            </Button>
          </DialogActions>
        </div>
      </Dialog>
    </div>
  )
}
