import { Button, Checkbox } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import ReactQuill from 'react-quill'
import { useDispatch, useSelector } from 'react-redux'
import { Card } from '../../components/Card/card'
import BoardImg from '../../multimedia/Board.png'
import './board.scss'
import { ModalCreatePlan } from './create-plan/create-plan'
import { IState } from '../../store/reducers'
import { CreateStrategicPlanAction } from '../../store/strategic-plan/actions/create.action'
import { StrategicPlanListAction } from '../../store/strategic-plan/actions/list.action'
import { StrategicPlanGetAction } from '../../store/strategic-plan/actions/get.action'

const Board = () => {
  const dispatch = useDispatch()
  const [viewModalCreatePlan, setViewModalCreatePlan] = useState(false)
  const history = useHistory()
  const { user } = useSelector((state: IState) => state.auth)
  const { listStrategicPlans, strategicPlan } = useSelector(
    (state: IState) => state.strategicPlan
  )

  useEffect(() => {
    if (!strategicPlan)
      dispatch(StrategicPlanGetAction.request(listStrategicPlans[0]?.id))
  }, [dispatch, listStrategicPlans])

  const onClickMacro1 = () => {
    history.push(`/board/macro1`)
  }
  const onClickMacro2 = () => {
    history.push(`/board/macro2`)
  }
  const onClickMacro3 = () => {
    history.push(`/board/macro3`)
  }
  const onClickMacro4 = () => {
    history.push(`/board/macro4`)
  }
  const onClickMacro5 = () => {
    history.push(`/board/macro5`)
  }
  const onClickMacro6 = () => {
    history.push(`/board/macro6`)
  }
  const onClickMacro7 = () => {
    history.push(`/board/macro7`)
  }
  const onClickMacro8 = () => {
    history.push(`/board/macro8`)
  }
  const onClickMacro9 = () => {
    history.push(`/board/macro9`)
  }
  const onClickMacro10 = () => {
    history.push(`/board/macro10`)
  }
  const onClickMacro11 = () => {
    history.push(`/board/macro11`)
  }
  const onClickMacro12 = () => {
    history.push(`/board/macro12`)
  }
  const onViewStrategicPlan = () => {
    history.push(`/strategic-plan`)
  }

  return (
    <div>
      <div className="board">
        {listStrategicPlans.length > 0 ? (
          <div className="board__fully">
            <div className="board__fully-title">Flujo Principal</div>
            <div className="board__fully-block">
              <Card
                macroActivity="Patrocinador del proyecto"
                onClickView={onClickMacro1}
              />
              <Card
                macroActivity="Workshop alineación y movilización de líderes"
                onClickView={onClickMacro2}
                isDone
              />
              <Card
                macroActivity="Propósito e identidad del proyecto"
                onClickView={onClickMacro3}
                isDone
              />
              <Card
                macroActivity="Clasificación de los interesados"
                onClickView={onClickMacro4}
                isDone
              />
            </div>
            <div className="board__fully-block">
              <Card
                macroActivity="Características de la cultura organizacional"
                onClickView={onClickMacro5}
                isDone
              />
              <Card
                macroActivity="Roles y responsabilidades"
                onClickView={onClickMacro6}
              />
              <Card
                macroActivity="Ambiente físico adecuado al proyecto"
                onClickView={onClickMacro7}
                isDone
              />
              <Card
                macroActivity="Asignación y desarrollo del equipo del proyecto"
                onClickView={onClickMacro8}
                isDone
              />
            </div>
            <div className="board__fully-block">
              <Card
                macroActivity="Predisposición del clima para cambios"
                onClickView={onClickMacro9}
                isDone
              />
              <Card
                macroActivity="Plan de Acción de Gestión del Cambio"
                onClickView={onClickMacro10}
                isDone
              />
              <Card
                macroActivity="Kick-off del proyecto"
                onClickView={onClickMacro11}
                isDone
              />
              <Card
                macroActivity="Plan Estratégico de Gestión del Cambio"
                onClickView={onClickMacro12}
                isDone
              />
            </div>
            <div className="board__fully__plan">
              <Button
                variant="outlined"
                className="board__fully__plan-button"
                onClick={onViewStrategicPlan}
              >
                VER PLAN ESTRATÉGICO
              </Button>
            </div>
          </div>
        ) : (
          <div className="board__empty">
            <img className="board__empty-img" src={BoardImg} alt="Board" />
            <h1 className="board__empty-text">
              No tiene ningún proyecto. Cree uno para comenzar.
            </h1>
            <Button
              variant="contained"
              className="board__empty-button"
              onClick={() => setViewModalCreatePlan(true)}
            >
              CREAR PLAN ESTRATÉGICO
            </Button>
          </div>
        )}
      </div>
      <ModalCreatePlan
        visible={viewModalCreatePlan}
        onClose={() => setViewModalCreatePlan(false)}
        idUser={user.id}
      ></ModalCreatePlan>
    </div>
  )
}

export default Board
