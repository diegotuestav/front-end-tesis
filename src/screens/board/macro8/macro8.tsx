import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import './macro8.scss'
import 'react-quill/dist/quill.snow.css'
import { Button } from '@material-ui/core'
import { Save } from '@material-ui/icons'
import ReactQuill from 'react-quill'
import { ModalActivity } from '../activity/activity'
import { IState } from '../../../store/reducers'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro8 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const [technician, setTechnician] = useState('')
  const [behavioral, setBehavioral] = useState('')
  const [training, setTraining] = useState('')
  const [plan, setPlan] = useState('')
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)

  useEffect(() => {
    if (macroactivity) {
      setTechnician(macroactivity.blankSpace1)
      setBehavioral(macroactivity.blankSpace2)
      setTraining(macroactivity.blankSpace3)
      setPlan(macroactivity.blankSpace4)
    }
  }, [macroactivity])

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan.id,
        idMacroActivityType: 8,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const onSaveMacroActivity = () => {
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity.id,
        blankSpace1: technician,
        blankSpace2: behavioral,
        blankSpace3: training,
        blankSpace4: plan,
        document1: null,
        document2: null,
      })
    )
  }

  const onViewActivity = () => {
    setViewModalActivity(true)
  }

  return (
    <div>
      <div className="macro8">
        <div className="macro8-content">
          <div className="macro8-title">
            Asignación y desarrollo del equipo de proyecto
          </div>
          <div className="macro8__section">
            <div className="macro8__subtitle">Asignación del equipo</div>
            <div className="macro8__double">
              <div className="macro8__section-sub">
                <div className="macro8__subtitle-sub">
                  Perfil técnico del equipo del proyecto
                </div>
                <ReactQuill
                  theme="snow"
                  onChange={setTechnician}
                  value={technician}
                  style={{ width: '1300px', height: '150px' }}
                  className="macro8__reactquill"
                />
              </div>
              <div className="macro8__section-sub">
                <div className="macro8__subtitle-sub">
                  Perfil comportamental del equipo del proyecto
                </div>
                <ReactQuill
                  theme="snow"
                  onChange={setBehavioral}
                  value={behavioral}
                  style={{ width: '1300px', height: '150px' }}
                  className="macro8__reactquill"
                />
              </div>
            </div>
          </div>
          <div className="macro8__section">
            <div className="macro8__subtitle">
              Entrenamientos preliminares del equipo
            </div>
            <div className="macro8__double">
              <div className="macro8__section-sub">
                <div className="macro8__subtitle-sub">
                  Entrenamientos previos para preparar al equipo
                </div>
                <ReactQuill
                  theme="snow"
                  onChange={setTraining}
                  value={training}
                  style={{ width: '1300px', height: '150px' }}
                  className="macro8__reactquill"
                />
              </div>
              <div className="macro8__section-sub">
                <div className="macro8__subtitle-sub">
                  Plan inicial de entrenamiento
                </div>
                <ReactQuill
                  theme="snow"
                  onChange={setPlan}
                  value={plan}
                  style={{ width: '1300px', height: '150px' }}
                  className="macro8__reactquill"
                />
              </div>
            </div>
          </div>
          <div className="macro8__activities">
            <div className="macro8__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro8__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro8__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>

        <div className="macro8__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro8__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}

export default Macro8
