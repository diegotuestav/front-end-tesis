import { Button } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import ReactQuill from 'react-quill'
import { useDispatch, useSelector } from 'react-redux'
import 'react-quill/dist/quill.snow.css'
import { Save } from '@material-ui/icons'
import './macro12.scss'
import { ModalActivity } from '../activity/activity'
import { IState } from '../../../store/reducers'
import { macroactivityGetAction } from '../../../store/macroactivity/actions/get.action'
import { macroactivityUpdateAction } from '../../../store/macroactivity/actions/update.action'
import { ActivityCard } from '../../../components/ActivityCard/activity-card'

const Macro12 = () => {
  const dispatch = useDispatch()
  const [viewModalActivity, setViewModalActivity] = useState(false)
  const [viewAddModalActivity, setViewAddModalActivity] = useState(false)
  const [quickWins, setQuickWins] = useState('')
  const [budget, setBudget] = useState('')
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { macroactivity } = useSelector((state: IState) => state.macroactivity)

  useEffect(() => {
    if (macroactivity) {
      setQuickWins(macroactivity.blankSpace1)
      setBudget(macroactivity.blankSpace2)
    }
  }, [macroactivity])

  useEffect(() => {
    dispatch(
      macroactivityGetAction.request({
        idStrategicPlan: strategicPlan.id,
        idMacroActivityType: 12,
      })
    )
  }, [dispatch, macroactivityGetAction, strategicPlan])

  const onSaveMacroActivity = () => {
    dispatch(
      macroactivityUpdateAction.request({
        idMacroActivity: macroactivity.id,
        blankSpace1: quickWins,
        blankSpace2: budget,
        blankSpace3: '',
        blankSpace4: '',
        document1: null,
        document2: null,
      })
    )
  }

  const onViewActivity = () => {
    setViewModalActivity(true)
  }
  return (
    <div>
      <div className="macro12">
        <div className="macro12-content">
          <div className="macro12-title">
            Plan Estratégico de Gestión del Cambio
          </div>

          <div className="macro12__section">
            <div className="macro12__subtitle">Quick-Wins</div>
            <ReactQuill
              theme="snow"
              onChange={setQuickWins}
              value={quickWins}
              style={{ width: '1300px', height: '150px' }}
              className="macro12__reactquill"
            />
          </div>
          <div className="macro12__section">
            <div className="macro12__subtitle">Presupuesto</div>
            <ReactQuill
              theme="snow"
              onChange={setBudget}
              value={budget}
              style={{ width: '1300px', height: '150px' }}
              className="macro12__reactquill"
            />
          </div>
          <div className="macro12__activities">
            <div className="macro12__subtitle">Actividades</div>
            <Button
              variant="outlined"
              className="macro12__activities-button"
              onClick={() => setViewAddModalActivity(true)}
            >
              Agregar
            </Button>
          </div>
          <div className="macro12__activities__list">
            {macroactivity?.activities.map((a, index) => (
              <ActivityCard
                activity={a}
                isDone={a.finished}
                onClickView={onViewActivity}
              />
            ))}
          </div>
        </div>
        <div className="macro12__save">
          <Button
            variant="contained"
            startIcon={<Save />}
            className="macro12__save-button"
            onClick={onSaveMacroActivity}
          >
            GUARDAR
          </Button>
        </div>
      </div>
      <ModalActivity
        visible={viewAddModalActivity}
        onClose={() => setViewAddModalActivity(false)}
        add
      ></ModalActivity>
      <ModalActivity
        visible={viewModalActivity}
        onClose={() => setViewModalActivity(false)}
      ></ModalActivity>
    </div>
  )
}
export default Macro12
