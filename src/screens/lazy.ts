import { lazy } from 'react'

const LazyIndicators = lazy(() => import('./indicator/indicators/indicators'))
const LazyQuiz = lazy(() => import('./indicator/quiz/quiz'))
const LazyQuizView = lazy(() => import('./indicator/quiz/quiz-view/quiz-view'))

const LazyBoard = lazy(() => import('./board/board'))
const LazyStrategicPlan = lazy(() => import('./strategic-plan/strategic-plan'))
const LazyMacro1 = lazy(() => import('./board/macro1/macro1'))
const LazyMacro2 = lazy(() => import('./board/macro2/macro2'))
const LazyMacro3 = lazy(() => import('./board/macro3/macro3'))
const LazyMacro4 = lazy(() => import('./board/macro4/macro4'))
const LazyMacro5 = lazy(() => import('./board/macro5/macro5'))
const LazyMacro6 = lazy(() => import('./board/macro6/macro6'))
const LazyMacro7 = lazy(() => import('./board/macro7/macro7'))
const LazyMacro8 = lazy(() => import('./board/macro8/macro8'))
const LazyMacro9 = lazy(() => import('./board/macro9/macro9'))
const LazyMacro10 = lazy(() => import('./board/macro10/macro10'))
const LazyMacro11 = lazy(() => import('./board/macro11/macro11'))
const LazyMacro12 = lazy(() => import('./board/macro12/macro12'))

const LazyCalendar = lazy(() => import('./calendar/calendar'))
const LazyTeam = lazy(() => import('./team/team'))

export {
  LazyIndicators,
  LazyQuiz,
  LazyQuizView,
  LazyBoard,
  LazyStrategicPlan,
  LazyCalendar,
  LazyTeam,
  LazyMacro1,
  LazyMacro2,
  LazyMacro3,
  LazyMacro4,
  LazyMacro5,
  LazyMacro6,
  LazyMacro7,
  LazyMacro8,
  LazyMacro9,
  LazyMacro10,
  LazyMacro11,
  LazyMacro12,
}
