import {
  AppBar,
  IconButton,
  Tabs,
  FormControl,
  NativeSelect,
  InputLabel,
} from '@material-ui/core'
import React, { useEffect, useState, Suspense } from 'react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import LogoutIcon from '@mui/icons-material/Logout'
import { useDispatch, useSelector } from 'react-redux'
import { Path } from '../common/constants/path.constants'
import History from '../common/History'
import {
  LazyBoard,
  LazyCalendar,
  LazyIndicators,
  LazyMacro1,
  LazyQuiz,
  LazyQuizView,
  LazyTeam,
  LazyMacro2,
  LazyMacro3,
  LazyMacro4,
  LazyMacro5,
  LazyMacro6,
  LazyMacro7,
  LazyMacro8,
  LazyMacro9,
  LazyMacro10,
  LazyMacro11,
  LazyMacro12,
  LazyStrategicPlan,
} from './lazy'
import './app-root.scss'
import { NewHeader } from '../components/NewHeader'
import Logo from '../multimedia/Logo.svg'
import { Login } from './login/login'
import { QuizResolved } from './indicator/quiz/quiz-resolved/quiz-resolved'
import { IState } from '../store/reducers'
import { authLogoutAction } from '../store/auth/actions/logout.actions'
import { StrategicPlanListAction } from '../store/strategic-plan/actions/list.action'
import { StrategicPlanGetAction } from '../store/strategic-plan/actions/get.action'

export const AppRoot = () => {
  const { user, error, loading } = useSelector((state: IState) => state.auth)
  History.listen(() => {
    window.scrollTo(0, 0)
  })

  const HeaderChildren = () => {
    const dispatch = useDispatch()
    const { listStrategicPlans, strategicPlan } = useSelector(
      (state: IState) => state.strategicPlan
    )

    const onLogout = () => {
      dispatch(authLogoutAction.request())
    }

    useEffect(() => {
      dispatch(StrategicPlanListAction.request(user.id))
    }, [dispatch, StrategicPlanListAction])

    const handleStrategicPlan = (event) => {
      dispatch(StrategicPlanGetAction.request(event.target.value))
    }

    return (
      <div>
        <div>
          <AppBar position="static">
            <Tabs aria-label="simple tabs example">
              {listStrategicPlans.length > 0 ? (
                <FormControl fullWidth className="comboBox">
                  <InputLabel variant="standard" htmlFor="uncontrolled-native">
                    Organización
                  </InputLabel>
                  <NativeSelect
                    defaultValue={strategicPlan?.id}
                    onChange={handleStrategicPlan}
                    inputProps={{
                      name: 'strategic-plan',
                      id: 'uncontrolled-native',
                    }}
                  >
                    {listStrategicPlans.map((sp, index) => (
                      <option value={sp?.id}>{sp?.organizationName}</option>
                    ))}
                  </NativeSelect>
                </FormControl>
              ) : (
                <div></div>
              )}
              <a className="left1" href={Path.BOARD}>
                Tablero
              </a>
              <a className="left2" href={Path.CALENDAR}>
                Calendario
              </a>
              <img className="logo" src={Logo} alt="Logo" />
              <a className="right3" href={Path.INDICATORS}>
                Indicadores
              </a>
              <a className="right4" href={Path.TEAM}>
                Equipo
              </a>
              <div className="hiddenfuture">
                <IconButton
                  aria-label="logout"
                  onClick={() => onLogout()}
                  className="app-main-container__logout"
                >
                  <LogoutIcon />
                </IconButton>
                <h4>{user.names}</h4>
              </div>
            </Tabs>
          </AppBar>
        </div>
      </div>
    )
  }

  const SuspenseLoading = () => {
    return (
      <div className="suspense-loading">
        <div color="#98a9bc" />
        Cargando página
      </div>
    )
  }

  const Routes = () => {
    const redirect = Path.BOARD
    return (
      <Switch>
        <Route path="/" exact>
          {!user ? <Login /> : <Redirect to={redirect} />}
        </Route>
        <Route path={Path.QUIZ_RESOLVED} exact>
          <QuizResolved />
        </Route>
        <Route path={Path.LOGIN} exact>
          {!user ? <Login /> : <Redirect to={redirect} />}
        </Route>
        <Route path={Path.LOGIN} exact component={Login} />
        <Route path={Path.INDICATORS} exact>
          {!user ? <Login /> : <LazyIndicators />}
        </Route>
        <Route path={Path.QUIZ} exact>
          {!user ? <Login /> : <LazyQuiz />}
        </Route>
        <Route path={Path.QUIZ_VIEW} exact>
          {!user ? <Login /> : <LazyQuizView />}
        </Route>
        <Route path={Path.BOARD} exact>
          {!user ? <Login /> : <LazyBoard />}
        </Route>
        <Route path={Path.STRATEGIC_PLAN} exact>
          {!user ? <Login /> : <LazyStrategicPlan />}
        </Route>
        <Route path={Path.MACRO1} exact>
          {!user ? <Login /> : <LazyMacro1 />}
        </Route>
        <Route path={Path.MACRO2} exact>
          {!user ? <Login /> : <LazyMacro2 />}
        </Route>
        <Route path={Path.MACRO3} exact>
          {!user ? <Login /> : <LazyMacro3 />}
        </Route>
        <Route path={Path.MACRO4} exact>
          {!user ? <Login /> : <LazyMacro4 />}
        </Route>
        <Route path={Path.MACRO5} exact>
          {!user ? <Login /> : <LazyMacro5 />}
        </Route>
        <Route path={Path.MACRO6} exact>
          {!user ? <Login /> : <LazyMacro6 />}
        </Route>
        <Route path={Path.MACRO7} exact>
          {!user ? <Login /> : <LazyMacro7 />}
        </Route>
        <Route path={Path.MACRO8} exact>
          {!user ? <Login /> : <LazyMacro8 />}
        </Route>
        <Route path={Path.MACRO9} exact>
          {!user ? <Login /> : <LazyMacro9 />}
        </Route>
        <Route path={Path.MACRO10} exact>
          {!user ? <Login /> : <LazyMacro10 />}
        </Route>
        <Route path={Path.MACRO11} exact>
          {!user ? <Login /> : <LazyMacro11 />}
        </Route>
        <Route path={Path.MACRO12} exact>
          {!user ? <Login /> : <LazyMacro12 />}
        </Route>
        <Route path={Path.CALENDAR} exact>
          {!user ? <Login /> : <LazyCalendar />}
        </Route>
        <Route path={Path.TEAM} exact>
          {!user ? <Login /> : <LazyTeam />}
        </Route>
      </Switch>
    )
  }

  return (
    <div className="app">
      <BrowserRouter>
        {user ? (
          <NewHeader>
            <HeaderChildren />
          </NewHeader>
        ) : (
          <div></div>
        )}
        <div className="app-main-container">
          <Suspense fallback={<SuspenseLoading />}>
            <Routes />
          </Suspense>
        </div>
      </BrowserRouter>
    </div>
  )
}
