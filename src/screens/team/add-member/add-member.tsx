import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Modal,
  TextField,
} from '@material-ui/core'
import React, { HTMLAttributes, useCallback, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { AddUserStrategicPlanAction } from '../../../store/strategic-plan/actions/add-user.action'
import './add-member.scss'

export interface ModalAddMemberProps extends HTMLAttributes<HTMLDivElement> {
  visible?: boolean
  onClose?: any
  idStrategicPlan?: number
}

export const ModalAddMember = ({
  visible,
  onClose,
  idStrategicPlan,
}: ModalAddMemberProps) => {
  const dispatch = useDispatch()
  const [email, setEmail] = useState('')

  const handleChange = (event) => {
    setEmail(event.target.value)
  }

  const onAddUser = () => {
    dispatch(
      AddUserStrategicPlanAction.request({
        idStrategicPlan,
        email,
      })
    )
    onClose()
  }

  return (
    <div className="add-member">
      <Dialog
        open={visible || false}
        onClose={onClose}
        fullWidth
        maxWidth="md"
        PaperProps={{
          style: {
            backgroundColor: '#F3F3F3',
          },
        }}
        aria-labelledby="form-dialog-title"
        className="add-member__dialog"
      >
        <div className="add-member__header">
          <Button onClick={onClose} className="add-member__button-x">
            X
          </Button>
        </div>
        <div className="add-member__content">
          <DialogTitle
            id="form-dialog-title"
            className="add-member__content-title"
          >
            Añadir Integrante de Equipo
          </DialogTitle>
          <DialogContent>
            <p className="add-member__content-field">Correo electrónico</p>
            <div className="add-member__content__div">
              <TextField
                fullWidth
                id="fullWidth"
                className="add-member__content__div-textfield"
                onChange={handleChange}
              />
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={onClose}
              variant="contained"
              className="add-member__button-cancel"
            >
              Cancelar
            </Button>
            <Button
              onClick={onAddUser}
              variant="contained"
              className="add-member__button-accept"
            >
              Añadir Integrante
            </Button>
          </DialogActions>
        </div>
      </Dialog>
    </div>
  )
}
