import { Button } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import MaterialTable from 'material-table'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IState } from '../../store/reducers'
import { StrategicPlanListUsersAction } from '../../store/strategic-plan/actions/list-users.action'
import { ModalAddMember } from './add-member/add-member'
import './team.scss'

const Team = () => {
  const dispatch = useDispatch()
  const [viewModalAddMember, setViewModalAddMember] = useState(false)
  const { strategicPlan } = useSelector((state: IState) => state.strategicPlan)
  const { user } = useSelector((state: IState) => state.auth)
  const { users } = useSelector((state: IState) => state.strategicPlan)

  const columnas = [
    {
      title: 'Nombre(s)',
      field: 'names',
    },
    {
      title: 'Apellido(s)',
      field: 'lastnames',
    },
    {
      title: 'Correo Electrónico',
      field: 'email',
    },
    {
      title: 'Teléfono',
      field: 'phone',
    },
  ]

  useEffect(() => {
    dispatch(StrategicPlanListUsersAction.request(strategicPlan?.id))
  }, [dispatch, strategicPlan])

  const data = users.map((u) => ({
    names: u.names,
    lastnames: u.lastnames,
    email: u.email,
    phone: u.phone,
  }))

  return (
    <div>
      <div className="team">
        <div className="team__title">Equipo de Gestión del Cambio</div>
        <div className="team__actions">
          <Button
            variant="contained"
            className="team__actions-button"
            startIcon={<Add />}
            onClick={() => setViewModalAddMember(true)}
          >
            Añadir Integrante
          </Button>
        </div>
        <div className="team__table">
          <MaterialTable
            columns={columnas}
            title=""
            data={data}
            actions={[
              {
                icon: 'delete',
                tooltip: 'Eliminar Vehículo',
                onClick: (event, rowData) =>
                  // eslint-disable-next-line
                  window.confirm(
                    'Estas seguro que deseas eliminar el vehiculo: '
                  ),
              },
            ]}
            options={{
              actionsColumnIndex: -1,
            }}
            localization={{
              header: {
                actions: 'Acciones',
              },
            }}
          />
        </div>
      </div>
      <ModalAddMember
        visible={viewModalAddMember}
        onClose={() => setViewModalAddMember(false)}
        idStrategicPlan={strategicPlan?.id}
      ></ModalAddMember>
    </div>
  )
}

export default Team
