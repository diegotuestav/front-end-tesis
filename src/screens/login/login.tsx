import {
  Button,
  FormControl,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
} from '@material-ui/core'
import { useHistory, useParams } from 'react-router-dom'
import {
  AccountCircle,
  Lock,
  Visibility,
  VisibilityOff,
} from '@material-ui/icons'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Logo from '../../multimedia/Logo.svg'
import { IState } from '../../store/reducers'
import './login.scss'
import { authLoginAction } from '../../store/auth/actions/login.actions'

export const Login = () => {
  const dispatch = useDispatch()
  const [isCorrect, setIsCorrect] = useState(true)
  const [email, setEmail] = useState('')
  const [auth, setAuth] = useState(true)
  const [password, setPassword] = useState('')
  const [showPassword, setShowPassword] = useState(false)
  const [userSelected, setUserSelected] = useState(null)
  const { user } = useSelector((state: IState) => state.auth)

  const onClickUser = () => {
    dispatch(authLoginAction.request({ email, password }))
  }

  useEffect(() => {
    setUserSelected(user)
  }, [user])

  const handleChangeEmail = (event) => {
    setEmail(event.target.value)
  }

  const handleChange = (event) => {
    setPassword(event.target.value)
  }

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword)
  }

  const handleMouseDownPassword = (event) => {
    event.preventDefault()
  }

  return (
    <div>
      <div className="login">
        <div className="login__container">
          <img className="login__container-logo" src={Logo} alt="Logo" />
          <div className="login__container-h1">Bienvenido/a</div>
          <div className="login__container-h2">
            Por favor, ingrese sus credenciales aquí
          </div>
          <div className="login__container-email">
            <FormControl variant="standard">
              <InputLabel htmlFor="input-with-icon-adornment">
                Correo electrónico
              </InputLabel>
              <Input
                id="input-with-icon-adornment"
                onChange={handleChangeEmail}
                startAdornment={
                  <InputAdornment position="start">
                    <AccountCircle />
                  </InputAdornment>
                }
              />
            </FormControl>
          </div>
          <div className="login__container-password">
            <FormControl variant="standard">
              <InputLabel htmlFor="standard-adornment-password">
                Contraseña
              </InputLabel>
              <Input
                id="standard-adornment-password"
                type={showPassword ? 'text' : 'password'}
                onChange={handleChange}
                startAdornment={
                  <InputAdornment position="start">
                    <Lock />
                  </InputAdornment>
                }
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
          </div>
          {isCorrect ? (
            <div></div>
          ) : (
            <div>
              <h3>No existe el usuario ingresado</h3>
            </div>
          )}
          <Button
            variant="contained"
            className="login__container-button"
            onClick={onClickUser}
          >
            Ingresar
          </Button>
        </div>
      </div>
    </div>
  )
}
