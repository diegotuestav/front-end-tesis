import { createAsyncAction, createReducer } from 'typesafe-actions'
import { IActivity } from '../../../interfaces/activity.interface'
import { ActivityActionTypes, activityInitialState } from '../types'

export const updateActivityAction = createAsyncAction(
  ActivityActionTypes.UPDATE_ACTIVITY,
  ActivityActionTypes.UPDATE_ACTIVITY_SUCCESS,
  ActivityActionTypes.UPDATE_ACTIVITY_FAIL
)<
  {
    idActivity: number
    title: string
    description: string
    notes: string
    endDate: Date
    finished: boolean
    users: number[]
    document: Blob[]
  },
  boolean,
  any
>()

export const updateActivityReducer = createReducer(activityInitialState)
  .handleAction(updateActivityAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(updateActivityAction.success, (state, action) => ({
    ...state,
    loading: false,
    updated: true,
  }))
  .handleAction(updateActivityAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
