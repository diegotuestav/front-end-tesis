import { createAsyncAction, createReducer } from 'typesafe-actions'
import { ActivityActionTypes, activityInitialState } from '../types'

export const addActivityAction = createAsyncAction(
  ActivityActionTypes.ADD_ACTIVITY,
  ActivityActionTypes.ADD_ACTIVITY_SUCCESS,
  ActivityActionTypes.ADD_ACTIVITY_FAIL
)<
  {
    idMacroActivity: number
    title: string
    description: string
    notes: string
    endDate: Date
    finished: boolean
    users: number[]
    document: Blob[]
  },
  boolean,
  any
>()

export const addActivityReducer = createReducer(activityInitialState)
  .handleAction(addActivityAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(addActivityAction.success, (state, action) => ({
    ...state,
    loading: false,
    added: true,
  }))
  .handleAction(addActivityAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
