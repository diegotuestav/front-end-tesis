import { createAsyncAction, createReducer } from 'typesafe-actions'
import { IActivity } from '../../../interfaces/activity.interface'
import { ActivityActionTypes, activityInitialState } from '../types'

export const activityLoadAction = createAsyncAction(
  ActivityActionTypes.LOAD_ACTIVITY,
  ActivityActionTypes.LOAD_ACTIVITY_SUCCESS,
  ActivityActionTypes.LOAD_ACTIVITY_FAIL
)<number, IActivity, any>()

export const activityLoadReducer = createReducer(activityInitialState)
  .handleAction(activityLoadAction.request, (state: any, action: any) => ({
    ...state,
    loading: true,
  }))
  .handleAction(activityLoadAction.success, (state: any, action: any) => ({
    ...state,
    loading: false,
    activity: action.payload,
  }))
  .handleAction(activityLoadAction.failure, (state: any, action: any) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
