import { IActivity } from '../../interfaces/activity.interface'

export enum ActivityActionTypes {
  LOAD_ACTIVITY = '[ACTIVITY] Load',
  LOAD_ACTIVITY_SUCCESS = '[ACTIVITY] Load Success',
  LOAD_ACTIVITY_FAIL = '[ACTIVITY] Load Fail',

  ADD_ACTIVITY = '[ACTIVITY] Add',
  ADD_ACTIVITY_SUCCESS = '[ACTIVITY] Add Success',
  ADD_ACTIVITY_FAIL = '[ACTIVITY] Add Fail',

  UPDATE_ACTIVITY = '[ACTIVITY] Update',
  UPDATE_ACTIVITY_SUCCESS = '[ACTIVITY] Update Success',
  UPDATE_ACTIVITY_FAIL = '[ACTIVITY] Update Fail',

  CLEAR = '[ACTIVITY] Clear',
}

export interface IActivityState {
  activity: IActivity
  loading: boolean
  error: any
  added: boolean
  updated: boolean
}

export const activityInitialState = {
  activity: null,
  loading: false,
  error: null,
  added: false,
  updated: false,
}
