import { StateObservable } from 'redux-observable'
import { from, Observable, of } from 'rxjs'
import { filter, switchMap, map, catchError } from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { macroactivityGetAction } from '../macroactivity/actions/get.action'
import { macroactivityGetByIdAction } from '../macroactivity/actions/getById.action'
// eslint-disable-next-line
import { IState } from '../reducers'
import { ActivityService } from '../services/activity.service'
import { addActivityAction } from './actions/add.actions'
import { activityLoadAction } from './actions/load.action'
import { updateActivityAction } from './actions/update.actions'

export const ActivityLoadEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(activityLoadAction.request)),
    switchMap(({ payload }) =>
      from(ActivityService.getActivity(payload)).pipe(
        map(activityLoadAction.success),
        catchError((error) => of(activityLoadAction.failure(error)))
      )
    )
  )

export const addActivityEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(addActivityAction.request)),
    switchMap(({ payload }) =>
      from(ActivityService.addActivity(payload)).pipe(
        map(addActivityAction.success),
        catchError((error) => of(addActivityAction.failure(error)))
      )
    )
  )

export const addActivityAfterSuccessEpic = (
  action$: Observable<any>,
  state$: StateObservable<IState>
) =>
  action$.pipe(
    filter(isActionOf(addActivityAction.success)),
    map(() =>
      macroactivityGetByIdAction.request(
        state$.value.macroactivity.macroactivity.id
      )
    )
  )

export const updateActivityEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(updateActivityAction.request)),
    switchMap(({ payload }) =>
      from(ActivityService.updateActivity(payload)).pipe(
        map(updateActivityAction.success),
        catchError((error) => of(updateActivityAction.failure(error)))
      )
    )
  )

export const updateActivityAfterSuccessEpic = (
  action$: Observable<any>,
  state$: StateObservable<IState>
) =>
  action$.pipe(
    filter(isActionOf(updateActivityAction.success)),
    map(() =>
      macroactivityGetByIdAction.request(
        state$.value.macroactivity.macroactivity.id
      )
    )
  )
