import reduceReducers from 'reduce-reducers'
import { combineEpics } from 'redux-observable'
import { addActivityReducer } from './actions/add.actions'
import { activityLoadReducer } from './actions/load.action'
import { updateActivityReducer } from './actions/update.actions'
// eslint-disable-next-line
import * as epics from './epics'
import { activityInitialState } from './types'

export const ActivityEpics = combineEpics(...Object.values(epics))

export const ActivityReducer = reduceReducers(
  activityInitialState,
  addActivityReducer,
  activityLoadReducer,
  updateActivityReducer
)
