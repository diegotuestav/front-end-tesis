import { ActivityModel } from '../../interfaces/activity.interface'
import {
  IMacroActivity,
  MacroActivityModel,
} from '../../interfaces/macroactivity.interface'
import { UserModel } from '../../interfaces/user.interface'
import { https } from '../../service/https'

export class MacroActivityService {
  static updateMacroActivity = async ({
    idMacroActivity,
    blankSpace1,
    blankSpace2,
    blankSpace3,
    blankSpace4,
    document1,
    document2,
  }): Promise<boolean> => {
    const updated = await https.post(`/macroactivity/update/`, {
      idMacroActivity,
      blankSpace1,
      blankSpace2,
      blankSpace3,
      blankSpace4,
      document1,
      document2,
    })
    return Promise.resolve(updated !== null)
  }

  static getMacroActivity = async (
    idStrategicPlan,
    idMacroActivityType
  ): Promise<IMacroActivity> => {
    var data
    data = await https.get(`/macroactivity/get/`, {
      params: {
        idStrategicPlan,
        idMacroActivityType,
      },
    })
    const macroactivity = MacroActivityService.mapMacroActivity(data.data)
    return Promise.resolve(macroactivity)
  }

  static getMacroActivityById = async (
    idMacroActivity
  ): Promise<IMacroActivity> => {
    var data
    data = await https.get(`/macroactivity/get/${idMacroActivity}`)
    const macroactivity = MacroActivityService.mapMacroActivity(data.data)
    return Promise.resolve(macroactivity)
  }

  static mapMacroActivity = (data: any): IMacroActivity => {
    const idMacroActivity = data.idMacroActivity
    const blankSpace1 = data.blankSpace1
    const blankSpace2 = data.blankSpace2
    const blankSpace3 = data.blankSpace3
    const blankSpace4 = data.blankSpace4
    const document1 = data.document1
    const document2 = data.document2

    const activities = data.activities.map(
      (activity) =>
        new ActivityModel(
          activity.idActivity,
          activity.title,
          activity.description,
          activity.notes,
          activity.endDate,
          activity.users.map(
            (user) =>
              new UserModel(
                user.idUser,
                user.names,
                user.lastnames,
                user.email,
                user.phone
              )
          ),
          activity.finished,
          activity.document
        )
    )

    const macroactivity = new MacroActivityModel(
      idMacroActivity,
      activities,
      blankSpace1,
      blankSpace2,
      blankSpace3,
      blankSpace4,
      document1,
      document2
    )

    return macroactivity
  }
}
