import {
  IIndicator,
  IndicatorModel,
} from '../../interfaces/indicator.interface'
import { https } from '../../service/https'

export class IndicatorsService {
  static getAll = async (idStrategicPlan): Promise<IIndicator[]> => {
    var data
    data = await https.get(`/indicator/list/${idStrategicPlan}`)
    const indicators = IndicatorsService.mapIndicators(data.data)
    return Promise.resolve(indicators)
  }

  static takeResults = async ({ idIndicator, answers }): Promise<boolean> => {
    const taken = await https.post(`/indicator/take-results/`, {
      idIndicator,
      answers,
    })
    return Promise.resolve(taken !== null)
  }

  static mapIndicators = (data: any): IIndicator[] => {
    const indicators = data.map(
      (indicator) =>
        new IndicatorModel(
          indicator.idIndicator,
          indicator.indicatorType.indicator,
          indicator.indicatorType.description,
          indicator.indicatorType.period,
          indicator.goal,
          indicator.indicatorType.formula,
          indicator.indicatorType.nameVariable1,
          indicator.variable1,
          indicator.indicatorType.variableDescription1,
          indicator.indicatorType.nameVariable2,
          indicator.variable2,
          indicator.indicatorType.variableDescription2,
          indicator.indicatorType.measureUnit,
          indicator.result
        )
    )
    return indicators
  }

  static updateIndicator = async ({ idIndicator, goal }): Promise<boolean> => {
    const updated = await https.post(
      `/indicator/update/${idIndicator}/`,
      {
        goal,
      },
      {
        params: {
          idIndicator,
        },
      }
    )
    return Promise.resolve(updated !== null)
  }
}
