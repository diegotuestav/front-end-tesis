import {
  ISponsorhip,
  SponsorhipModel,
} from '../../interfaces/sponsorship.interface'
import { https } from '../../service/https'

export class SponsorshipService {
  static addSponsorship = async ({
    names,
    lastnames,
    email,
    phone,
    charge,
    idRole,
    idStrategicPlan,
  }): Promise<boolean> => {
    const added = await https.post(`/sponsorship/create/`, {
      names,
      lastnames,
      email,
      phone,
      charge,
      idRole,
      idStrategicPlan,
    })
    return Promise.resolve(added !== null)
  }

  static updateSponsorship = async ({
    idSponsorship,
    names,
    lastnames,
    email,
    phone,
    charge,
    idRole,
    idStrategicPlan,
  }): Promise<boolean> => {
    const updated = await https.post(`/sponsorship/update/${idSponsorship}`, {
      names,
      lastnames,
      email,
      phone,
      charge,
      idRole,
      idStrategicPlan,
    })
    return Promise.resolve(updated !== null)
  }

  static getSponsorships = async (idStrategicPlan): Promise<ISponsorhip[]> => {
    var data
    data = await https.get(`/sponsorship/list/${idStrategicPlan}`)
    const sponsorships = SponsorshipService.mapSponsorship(data.data)
    return Promise.resolve(sponsorships)
  }

  static mapSponsorship = (data: any): ISponsorhip[] => {
    const sponsorships = data.map(
      (sponsorship) =>
        new SponsorhipModel(
          sponsorship.idSponsorship,
          sponsorship.names,
          sponsorship.lastnames,
          sponsorship.email,
          sponsorship.phone,
          sponsorship.charge,
          sponsorship.role.roleName
        )
    )
    return sponsorships
  }
}
