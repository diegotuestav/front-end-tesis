import { QuestionModel } from '../../interfaces/question.interface'
import { IQuiz, QuizModel } from '../../interfaces/quiz.interface'
import { AlternativeModel } from '../../interfaces/alternative.interface'
import { https } from '../../service/https'

export class QuizService {
  static getQuiz = async (idIndicator: number): Promise<IQuiz> => {
    var data
    data = await https.get(`/quiz/get/${idIndicator}`)
    const quiz = QuizService.mapQuiz(data.data)
    return Promise.resolve(quiz)
  }

  static mapQuiz = (data): IQuiz => {
    const title = data.title
    const introduction = data.introduction

    const questions = data.questions.map(
      (question) =>
        new QuestionModel(
          question.id,
          question.statement,
          question.numQuestion,
          question.score,
          question.alternatives.map(
            (alternative) =>
              new AlternativeModel(
                alternative.id,
                alternative.alternative,
                alternative.isCorrect
              )
          )
        )
    )

    const quiz = new QuizModel(title, introduction, questions)
    return quiz
  }

  static updateQuiz = async ({
    id_indicator,
    title,
    introduction,
    questions,
  }): Promise<boolean> => {
    const updated = await https.post(`/quiz/update/`, {
      id_indicator,
      title,
      introduction,
      questions,
    })
    return Promise.resolve(updated !== null)
  }
}
