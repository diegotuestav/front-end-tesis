import { MacroActivityModel } from '../../interfaces/macroactivity.interface'
import {
  IStrategicPlan,
  StrategicPlanModel,
} from '../../interfaces/strategic-plan.interface'
import { IUser, UserModel } from '../../interfaces/user.interface'
import { https } from '../../service/https'
import { StorageService } from './storage.service'

export class StrategicPlanService {
  static KEY_STRATEGIC_PLAN = 'strategic-plan'

  static createStrategicPlan = async ({
    idUser,
    organizationName,
  }): Promise<boolean> => {
    const created = await https.post(`/strategic-plan/create/`, {
      idUser,
      organizationName,
    })
    const strategicPlan = StrategicPlanService.mapStrategicPlan(created.data)
    await StorageService.set(
      StrategicPlanService.KEY_STRATEGIC_PLAN,
      strategicPlan,
      true
    )
    return Promise.resolve(created !== null)
  }

  static getAll = async (idUser): Promise<IStrategicPlan[]> => {
    var data
    data = await https.get(`/strategic-plan/list/${idUser}`)
    const strategicPlans = StrategicPlanService.mapStrategicPlans(data.data)
    return Promise.resolve(strategicPlans)
  }

  static mapStrategicPlans = (data: any): IStrategicPlan[] => {
    const strategicPlans = data.map(
      (plan) =>
        new StrategicPlanModel(
          plan.idStrategicPlan,
          plan.organizationName,
          null
        )
    )
    return strategicPlans
  }

  static getStrategicPlan = async (
    idStrategicPlan
  ): Promise<IStrategicPlan> => {
    var data
    data = await https.get(`/strategic-plan/get/${idStrategicPlan}`)
    const strategicPlan = StrategicPlanService.mapStrategicPlan(data.data)
    await StorageService.set(
      StrategicPlanService.KEY_STRATEGIC_PLAN,
      strategicPlan,
      true
    )
    return Promise.resolve(strategicPlan)
  }

  static mapStrategicPlan = (data: any): IStrategicPlan => {
    const idStrategicPlan = data.idStrategicPlan
    const organizationName = data.organizationName

    const macroActivities = data.macroActivities.map(
      (macroActivitie) =>
        new MacroActivityModel(
          macroActivitie.id,
          null,
          macroActivitie.blankSpace1,
          macroActivitie.blankSpace2,
          macroActivitie.blankSpace3,
          macroActivitie.blankSpace4,
          macroActivitie.document1,
          macroActivitie.document2
        )
    )

    const strategicPlan = new StrategicPlanModel(
      idStrategicPlan,
      organizationName,
      macroActivities
    )
    return strategicPlan
  }

  static load = async (): Promise<{ strategicPlan: IStrategicPlan }> => {
    const strategicPlan = await StorageService.get(
      StrategicPlanService.KEY_STRATEGIC_PLAN
    )
    if (!strategicPlan) await Promise.reject()
    return Promise.resolve({ strategicPlan })
  }

  static getUsers = async (idStrategicPlan): Promise<IUser[]> => {
    var data
    data = await https.get(`/strategic-plan/list-users/${idStrategicPlan}`)
    const users = StrategicPlanService.mapUsers(data.data)
    return Promise.resolve(users)
  }

  static mapUsers = (data): IUser[] => {
    const users = data.map(
      (user) =>
        new UserModel(
          user.idUser,
          user.names,
          user.lastnames,
          user.email,
          user.phone,
          user.photo
        )
    )
    return users
  }

  static addUser = async ({ idStrategicPlan, email }): Promise<boolean> => {
    const added = await https.post(`/strategic-plan/add-user/`, {
      idStrategicPlan,
      email,
    })
    return Promise.resolve(added !== null)
  }
}
