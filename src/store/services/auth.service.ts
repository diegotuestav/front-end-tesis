import { IUser, UserModel } from '../../interfaces/user.interface'
import { https } from '../../service/https'
import { StorageService } from './storage.service'

export class AuthService {
  static KEY_USER = 'user'

  static getUser = async (
    email: string,
    password: string
  ): Promise<{ user: IUser }> => {
    var data
    data = await https.get('user/get-login/', {
      params: {
        email,
        password,
      },
    })

    const user = AuthService.mapUser(data.data)
    await StorageService.set(AuthService.KEY_USER, user, true)
    return Promise.resolve({ user })
  }

  static mapUser = (data): IUser => {
    const user = new UserModel(
      data.idUser,
      data.names,
      data.lastnames,
      data.email,
      data.phone
    )

    return user
  }

  static load = async (): Promise<{ user: IUser }> => {
    const user = await StorageService.get(AuthService.KEY_USER)
    if (!user) await Promise.reject()
    return Promise.resolve({ user })
  }

  static logout = async (): Promise<boolean> => {
    StorageService.clear()
    return Promise.resolve(true)
  }
}
