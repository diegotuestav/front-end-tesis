import { ActivityModel, IActivity } from '../../interfaces/activity.interface'
import { UserModel } from '../../interfaces/user.interface'
import { https } from '../../service/https'

export class ActivityService {
  static addActivity = async ({
    idMacroActivity,
    title,
    description,
    notes,
    endDate,
    finished,
    users,
    document,
  }): Promise<boolean> => {
    const added = await https.post(`/activity/create/`, {
      idMacroActivity,
      title,
      description,
      notes,
      endDate,
      finished,
      users,
      document,
    })
    return Promise.resolve(added !== null)
  }

  static updateActivity = async ({
    idActivity,
    title,
    description,
    notes,
    endDate,
    finished,
    users,
    document,
  }): Promise<boolean> => {
    const updated = await https.post(`/activity/update/`, {
      idActivity,
      title,
      description,
      notes,
      endDate,
      finished,
      users,
      document,
    })
    return Promise.resolve(updated !== null)
  }

  static getActivity = async (idActivity): Promise<IActivity> => {
    var data
    data = await https.get(`/activity/get/${idActivity}`)
    const activity = ActivityService.mapActivity(data.data)
    return Promise.resolve(activity)
  }

  static mapActivity = (data: any): IActivity => {
    const idActivity = data.idActivity
    const title = data.title
    const description = data.description
    const notes = data.notes
    const endDate = data.endDate
    const users = data.users.map(
      (user) =>
        new UserModel(
          user.idUser,
          user.names,
          user.lastnames,
          user.email,
          user.phone
        )
    )
    const finished = data.finished
    const document = data.document

    const activity = new ActivityModel(
      idActivity,
      title,
      description,
      notes,
      endDate,
      users,
      finished,
      document
    )

    return activity
  }
}
