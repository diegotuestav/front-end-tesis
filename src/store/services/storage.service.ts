export class StorageService {
  static set = (name: string, value: any, persist = false): Promise<void> => {
    return Promise.resolve().then(() => {
      if (persist) localStorage.setItem(name, JSON.stringify(value))
      else sessionStorage.setItem(name, JSON.stringify(value))
    })
  }

  static get = (name: string): Promise<any> => {
    return Promise.resolve().then(() => {
      const local = localStorage.getItem(name)

      if (local) return JSON.parse(local)

      return JSON.parse(sessionStorage.getItem(name) as string)
    })
  }

  static clear = () => {
    localStorage.clear()
    sessionStorage.clear()
  }
}
