import {
  CollectiveModel,
  ICollective,
} from '../../interfaces/collective.interface'
import {
  IStakeholder,
  StakeholderModel,
} from '../../interfaces/stakeholder.interface'
import { https } from '../../service/https'

export class StakeholderService {
  static addStakeholder = async ({
    names,
    lastnames,
    email,
    phone,
    idProfile,
    idAdhesion,
    idCollective,
    idStrategicPlan,
  }): Promise<boolean> => {
    const added = await https.post(`/stakeholder/create/`, {
      names,
      lastnames,
      email,
      phone,
      idProfile,
      idAdhesion,
      idCollective,
      idStrategicPlan,
    })
    return Promise.resolve(added !== null)
  }

  static getStakeholders = async (idStrategicPlan): Promise<IStakeholder[]> => {
    var data
    data = await https.get(`/stakeholder/list/${idStrategicPlan}`)
    const stakeholders = StakeholderService.mapStakeholder(data.data)
    return Promise.resolve(stakeholders)
  }

  static mapStakeholder = (data: any): IStakeholder[] => {
    const stakeholders = data.map(
      (stakeholder) =>
        new StakeholderModel(
          stakeholder.idStakeholder,
          stakeholder.names,
          stakeholder.lastnames,
          stakeholder.email,
          stakeholder.phone,
          stakeholder.profile.profile,
          stakeholder.adhesion.adhesion,
          stakeholder.collective.collective
        )
    )
    return stakeholders
  }

  static getCollectives = async (idStrategicPlan): Promise<ICollective[]> => {
    var data
    data = await https.get(`/collective/list/${idStrategicPlan}`)
    const collectives = StakeholderService.mapCollectives(data.data)
    return Promise.resolve(collectives)
  }

  static mapCollectives = (data: any): ICollective[] => {
    const collectives = data.map(
      (collective) =>
        new CollectiveModel(collective.idCollective, collective.collective)
    )
    return collectives
  }
}
