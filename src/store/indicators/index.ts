import reduceReducers from 'reduce-reducers'
import { combineEpics } from 'redux-observable'
import { indicatorsLoadReducer } from './actions/load.action'
import { takenResultsReducer } from './actions/take-results.action'
import { updateIndicatorReducer } from './actions/update.actions'
// eslint-disable-next-line
import * as epics from './epics'
import { indicatorsInitialState } from './types'

export const IndicatorsEpics = combineEpics(...Object.values(epics))

export const IndicatorsReducer = reduceReducers(
  indicatorsInitialState,
  indicatorsLoadReducer,
  updateIndicatorReducer,
  takenResultsReducer
)
