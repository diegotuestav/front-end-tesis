import { createAsyncAction, createReducer } from 'typesafe-actions'
import { IndicatorActionTypes, indicatorsInitialState } from '../types'

export const takeResultsAction = createAsyncAction(
  IndicatorActionTypes.TAKE_RESULTS,
  IndicatorActionTypes.TAKE_RESULTS_SUCCESS,
  IndicatorActionTypes.TAKE_RESULTS_FAIL
)<{ idIndicator: number; answers: string[] }, boolean, any>()

export const takenResultsReducer = createReducer(indicatorsInitialState)
  .handleAction(takeResultsAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(takeResultsAction.success, (state, action) => ({
    ...state,
    loading: false,
    taken: true,
  }))
  .handleAction(takeResultsAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
