import { createAsyncAction, createReducer } from 'typesafe-actions'
import { IndicatorActionTypes, indicatorsInitialState } from '../types'

export const updateIndicatorAction = createAsyncAction(
  IndicatorActionTypes.UPDATE_INDICATOR,
  IndicatorActionTypes.UPDATE_INDICATOR_SUCCESS,
  IndicatorActionTypes.UPDATE_INDICATOR_FAIL
)<{ idIndicator: number; goal: number }, boolean, any>()

export const updateIndicatorReducer = createReducer(indicatorsInitialState)
  .handleAction(updateIndicatorAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(updateIndicatorAction.success, (state, action) => ({
    ...state,
    loading: false,
    updated: true,
  }))
  .handleAction(updateIndicatorAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
