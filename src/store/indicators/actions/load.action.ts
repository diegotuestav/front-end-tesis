import { createAsyncAction, createReducer } from 'typesafe-actions'
import { IndicatorActionTypes, indicatorsInitialState } from '../types'
import { IIndicator } from '../../../interfaces/indicator.interface'

export const indicatorsLoadAction = createAsyncAction(
  IndicatorActionTypes.LOAD_INDICATOR,
  IndicatorActionTypes.LOAD_INDICATOR_SUCCESS,
  IndicatorActionTypes.LOAD_INDICATOR_FAIL
)<number, IIndicator[], any>()

export const indicatorsLoadReducer = createReducer(indicatorsInitialState)
  .handleAction(indicatorsLoadAction.request, (state: any, action: any) => ({
    ...state,
    loading: true,
  }))
  .handleAction(indicatorsLoadAction.success, (state: any, action: any) => ({
    ...state,
    loading: false,
    indicators: action.payload,
  }))
  .handleAction(indicatorsLoadAction.failure, (state: any, action: any) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
