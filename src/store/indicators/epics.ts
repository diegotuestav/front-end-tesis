import { StateObservable } from 'redux-observable'
import { from, Observable, of } from 'rxjs'
import { filter, switchMap, map, catchError } from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
// eslint-disable-next-line
import { IState } from '../reducers'
import { IndicatorsService } from '../services/indicators.service'
import { indicatorsLoadAction } from './actions/load.action'
import { takeResultsAction } from './actions/take-results.action'
import { updateIndicatorAction } from './actions/update.actions'

export const indicatorsLoadEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(indicatorsLoadAction.request)),
    switchMap(({ payload }) =>
      from(IndicatorsService.getAll(payload)).pipe(
        map(indicatorsLoadAction.success),
        catchError((error) => of(indicatorsLoadAction.failure(error)))
      )
    )
  )

export const updateIndicatorEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(updateIndicatorAction.request)),
    switchMap(({ payload }) =>
      from(IndicatorsService.updateIndicator(payload)).pipe(
        map(updateIndicatorAction.success),
        catchError((error) => of(updateIndicatorAction.failure(error)))
      )
    )
  )

export const takeResultsEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(takeResultsAction.request)),
    switchMap(({ payload }) =>
      from(IndicatorsService.takeResults(payload)).pipe(
        map(takeResultsAction.success),
        catchError((error) => of(takeResultsAction.failure(error)))
      )
    )
  )

export const updateIndicatorAfterSuccessEpic = (
  action$: Observable<any>,
  state$: StateObservable<IState>
) =>
  action$.pipe(
    filter(isActionOf(updateIndicatorAction.success)),
    map(() =>
      indicatorsLoadAction.request(state$.value.strategicPlan.strategicPlan.id)
    )
  )
