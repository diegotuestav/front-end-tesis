import { IIndicator } from '../../interfaces/indicator.interface'

export enum IndicatorActionTypes {
  LOAD_INDICATOR = '[INDICATORS] Load',
  LOAD_INDICATOR_SUCCESS = '[INDICATORS] Load Success',
  LOAD_INDICATOR_FAIL = '[INDICATORS] Load Fail',

  UPDATE_INDICATOR = '[INDICATORS] Update',
  UPDATE_INDICATOR_SUCCESS = '[INDICATORS] Update Success',
  UPDATE_INDICATOR_FAIL = '[INDICATORS] Update Fail',

  TAKE_RESULTS = '[INDICATORS] Take Results',
  TAKE_RESULTS_SUCCESS = '[INDICATORS] Take Results Success',
  TAKE_RESULTS_FAIL = '[INDICATORS] Take Results Fail',

  CLEAR = '[INDICATORS] Clear',
}

export interface IIndicatorsState {
  indicators: IIndicator[]
  loading: boolean
  error: any
  updated: boolean
  taken: boolean
}

export const indicatorsInitialState = {
  indicators: [],
  loading: false,
  error: null,
  updated: false,
  taken: false,
}
