import { createAsyncAction, createReducer } from 'typesafe-actions'
import { IUser } from '../../../interfaces/user.interface'
import { AuthActionTypes, authInitialState } from '../types'

export const authLoginAction = createAsyncAction(
  AuthActionTypes.LOGIN,
  AuthActionTypes.LOGIN_SUCCESS,
  AuthActionTypes.LOGIN_FAIL
)<{ email: string; password: string }, { user: IUser }, any>()

export const authLoginReducer = createReducer(authInitialState)
  .handleAction(authLoginAction.request, (state, action) => ({
    ...state,
    loading: true,
    error: null,
  }))
  .handleAction(authLoginAction.success, (state, action) => ({
    ...state,
    loading: false,
    user: action.payload.user,
    error: null,
  }))
  .handleAction(authLoginAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
