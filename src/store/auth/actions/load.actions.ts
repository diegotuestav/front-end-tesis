import { createAsyncAction, createReducer } from 'typesafe-actions'
import { IUser } from '../../../interfaces/user.interface'
import { AuthActionTypes, authInitialState } from '../types'

export const authLoadAction = createAsyncAction(
  AuthActionTypes.LOAD,
  AuthActionTypes.LOAD_SUCCESS,
  AuthActionTypes.LOAD_FAIL
)<void, IUser, any>()

export const authLoadReducer = createReducer(authInitialState)
  .handleAction(authLoadAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(authLoadAction.success, (state, action) => ({
    ...state,
    loading: false,
    user: action.payload,
  }))
  .handleAction(authLoadAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
