import { createAsyncAction, createReducer } from 'typesafe-actions'
import { AuthActionTypes, authInitialState } from '../types'

export const authLogoutAction = createAsyncAction(
  AuthActionTypes.LOGOUT,
  AuthActionTypes.LOGOUT_SUCCESS,
  AuthActionTypes.LOGOUT_FAIL
)<void, void, any>()

export const authLogoutReducer = createReducer(authInitialState)
  .handleAction(authLogoutAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(authLogoutAction.success, (state, action) => ({
    ...state,
    user: null,
    loading: false,
  }))
  .handleAction(authLogoutAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
