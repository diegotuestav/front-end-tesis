import { IUser } from '../../interfaces/user.interface'

export enum AuthActionTypes {
  LOGIN = '[AUTH] Login',
  LOGIN_SUCCESS = '[AUTH] Login success',
  LOGIN_FAIL = '[AUTH] Login fail',

  LOGOUT = '[AUTH] Logout',
  LOGOUT_SUCCESS = '[AUTH] Logout success',
  LOGOUT_FAIL = '[AUTH] Logout fail',

  LOAD = '[AUTH] Load',
  LOAD_SUCCESS = '[AUTH] Load success',
  LOAD_FAIL = '[AUTH] Load fail',
}

export interface IAuthState {
  user: IUser
  loading: boolean
  error: any
}

export const authInitialState = {
  user: null,
  loading: true,
  error: null,
}
