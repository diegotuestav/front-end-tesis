import reduceReducers from 'reduce-reducers'
import { combineEpics } from 'redux-observable'
import { authLoadReducer } from './actions/load.actions'
import { authLoginReducer } from './actions/login.actions'
import { authLogoutReducer } from './actions/logout.actions'
import * as epics from './epics'
import { authInitialState } from './types'

export const AuthEpics = combineEpics(...Object.values(epics))

export const AuthReducer = reduceReducers(
  authInitialState,
  authLoginReducer,
  authLogoutReducer,
  authLoadReducer
)
