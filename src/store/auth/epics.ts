import { empty, from, Observable, of } from 'rxjs'
import { catchError, filter, map, switchMap } from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import History from '../../common/History'
import { Path } from '../../common/constants/path.constants'
import { AuthService } from '../services/auth.service'
import { authLoadAction } from './actions/load.actions'
import { authLoginAction } from './actions/login.actions'
import { authLogoutAction } from './actions/logout.actions'

export const authLoginEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(authLoginAction.request)),
    switchMap(({ payload }) =>
      from(AuthService.getUser(payload.email, payload.password)).pipe(
        map(authLoginAction.success),
        catchError((error) => of(authLoginAction.failure(error)))
      )
    )
  )

export const authLoginSuccessEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(authLoginAction.success)),
    switchMap(() => {
      History.push(Path.BOARD)
      return empty()
    })
  )

export const authLoadEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(authLoadAction.request)),
    switchMap(() =>
      from(AuthService.load()).pipe(
        map(({ user }) => authLoadAction.success(user as any)),
        catchError((error) => of(authLoadAction.failure(error)))
      )
    )
  )

export const authLogoutEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(authLogoutAction.request)),
    switchMap(() =>
      from(AuthService.logout()).pipe(
        map(() => authLogoutAction.success()),
        catchError((error) => of(authLogoutAction.failure(error)))
      )
    )
  )

export const authLogoutSuccessEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(authLogoutAction.success)),
    switchMap(() => {
      History.push(Path.LOGIN)
      return empty()
    })
  )
