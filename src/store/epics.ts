import { combineEpics, createEpicMiddleware } from 'redux-observable'
import { ActivityEpics } from './activity'
import { AuthEpics } from './auth'
import { IndicatorsEpics } from './indicators'
import { MacroActivityEpics } from './macroactivity'
import { QuizEpics } from './quiz'
import { SponsorshipEpics } from './sponsorship'
import { StakeholderEpics } from './stakeholders'
import { StrategicPlanEpics } from './strategic-plan'

export const RootEpic = combineEpics(
  IndicatorsEpics,
  QuizEpics,
  AuthEpics,
  StrategicPlanEpics,
  MacroActivityEpics,
  StakeholderEpics,
  SponsorshipEpics,
  ActivityEpics
)

export const epicMiddleware = createEpicMiddleware()
