import { from, Observable, of } from 'rxjs'
import { filter, switchMap, map, catchError } from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { macroactivityGetAction } from './actions/get.action'
import { macroactivityUpdateAction } from './actions/update.action'
import { MacroActivityService } from '../services/macroactivity.service'
import { macroactivityGetByIdAction } from './actions/getById.action'

export const MacroActivityGetEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(macroactivityGetAction.request)),
    switchMap(({ payload }) =>
      from(
        MacroActivityService.getMacroActivity(
          payload.idStrategicPlan,
          payload.idMacroActivityType
        )
      ).pipe(
        map(macroactivityGetAction.success),
        catchError((error) => of(macroactivityGetAction.failure(error)))
      )
    )
  )

export const MacroActivityGetByIdEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(macroactivityGetByIdAction.request)),
    switchMap(({ payload }) =>
      from(MacroActivityService.getMacroActivityById(payload)).pipe(
        map(macroactivityGetByIdAction.success),
        catchError((error) => of(macroactivityGetByIdAction.failure(error)))
      )
    )
  )

export const UpdateMacroActivityEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(macroactivityUpdateAction.request)),
    switchMap(({ payload }) =>
      from(MacroActivityService.updateMacroActivity(payload)).pipe(
        map(macroactivityUpdateAction.success),
        catchError((error) => of(macroactivityUpdateAction.failure(error)))
      )
    )
  )
