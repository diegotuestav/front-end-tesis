import { createAsyncAction, createReducer } from 'typesafe-actions'
import { MacroActivityActionTypes, macroActivityInitialState } from '../types'
import { IMacroActivity } from '../../../interfaces/macroactivity.interface'

export const macroactivityGetByIdAction = createAsyncAction(
  MacroActivityActionTypes.GET_MACROACTIVITY,
  MacroActivityActionTypes.GET_MACROACTIVITY_SUCCESS,
  MacroActivityActionTypes.GET_MACROACTIVITY_FAIL
)<number, IMacroActivity, any>()

export const macroactivityGetByIdReducer = createReducer(
  macroActivityInitialState
)
  .handleAction(
    macroactivityGetByIdAction.request,
    (state: any, action: any) => ({
      ...state,
      loading: true,
    })
  )
  .handleAction(
    macroactivityGetByIdAction.success,
    (state: any, action: any) => ({
      ...state,
      loading: false,
      macroactivity: action.payload,
    })
  )
  .handleAction(
    macroactivityGetByIdAction.failure,
    (state: any, action: any) => ({
      ...state,
      loading: false,
      error: action.payload,
    })
  )
