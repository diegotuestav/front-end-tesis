import { createAsyncAction, createReducer } from 'typesafe-actions'
import { MacroActivityActionTypes, macroActivityInitialState } from '../types'
import { IMacroActivity } from '../../../interfaces/macroactivity.interface'

export const macroactivityGetAction = createAsyncAction(
  MacroActivityActionTypes.GET_MACROACTIVITY,
  MacroActivityActionTypes.GET_MACROACTIVITY_SUCCESS,
  MacroActivityActionTypes.GET_MACROACTIVITY_FAIL
)<
  { idStrategicPlan: number; idMacroActivityType: number },
  IMacroActivity,
  any
>()

export const macroactivityGetReducer = createReducer(macroActivityInitialState)
  .handleAction(macroactivityGetAction.request, (state: any, action: any) => ({
    ...state,
    loading: true,
  }))
  .handleAction(macroactivityGetAction.success, (state: any, action: any) => ({
    ...state,
    loading: false,
    macroactivity: action.payload,
  }))
  .handleAction(macroactivityGetAction.failure, (state: any, action: any) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
