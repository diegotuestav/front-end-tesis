import { createAsyncAction, createReducer } from 'typesafe-actions'
import { MacroActivityActionTypes, macroActivityInitialState } from '../types'
import { IMacroActivity } from '../../../interfaces/macroactivity.interface'

export const macroactivityUpdateAction = createAsyncAction(
  MacroActivityActionTypes.UPDATE_MACROACTIVITY,
  MacroActivityActionTypes.UPDATE_MACROACTIVITY_SUCCESS,
  MacroActivityActionTypes.UPDATE_MACROACTIVITY_FAIL
)<
  {
    idMacroActivity: number
    blankSpace1: string
    blankSpace2: string
    blankSpace3: string
    blankSpace4: string
    document1: Blob
    document2: Blob
  },
  boolean,
  any
>()

export const macroactivityUpdateReducer = createReducer(
  macroActivityInitialState
)
  .handleAction(
    macroactivityUpdateAction.request,
    (state: any, action: any) => ({
      ...state,
      loading: true,
    })
  )
  .handleAction(
    macroactivityUpdateAction.success,
    (state: any, action: any) => ({
      ...state,
      loading: false,
      updated: action.payload,
    })
  )
  .handleAction(
    macroactivityUpdateAction.failure,
    (state: any, action: any) => ({
      ...state,
      loading: false,
      error: action.payload,
    })
  )
