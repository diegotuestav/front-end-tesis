import reduceReducers from 'reduce-reducers'
import { combineEpics } from 'redux-observable'
import * as epics from './epics'
import { macroactivityGetReducer } from './actions/get.action'
import { macroactivityUpdateReducer } from './actions/update.action'
import { macroActivityInitialState } from './types'
import { macroactivityGetByIdReducer } from './actions/getById.action'

export const MacroActivityEpics = combineEpics(...Object.values(epics))

export const MacroActivityReducer = reduceReducers(
  macroActivityInitialState,
  macroactivityUpdateReducer,
  macroactivityGetReducer,
  macroactivityGetByIdReducer
)
