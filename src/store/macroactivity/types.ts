import { IMacroActivity } from '../../interfaces/macroactivity.interface'

export enum MacroActivityActionTypes {
  GET_MACROACTIVITY = '[MACROACTIVITY] Get',
  GET_MACROACTIVITY_SUCCESS = '[MACROACTIVITY] Get Success',
  GET_MACROACTIVITY_FAIL = '[MACROACTIVITY] Get Fail',

  GET_MACROACTIVITY_ID = '[MACROACTIVITY] Get By Id',
  GET_MACROACTIVITY_ID_SUCCESS = '[MACROACTIVITY] Get By Id Success',
  GET_MACROACTIVITY_ID_FAIL = '[MACROACTIVITY] Get By Id Fail',

  UPDATE_MACROACTIVITY = '[MACROACTIVITY] Update',
  UPDATE_MACROACTIVITY_SUCCESS = '[MACROACTIVITY] Update Success',
  UPDATE_MACROACTIVITY_FAIL = '[MACROACTIVITY] Update Fail',

  CLEAR = '[MACROACTIVITY] Clear',
}

export interface IMacroActivityState {
  macroactivity: IMacroActivity
  loading: boolean
  error: any
  updated: boolean
}

export const macroActivityInitialState = {
  macroactivity: null,
  loading: false,
  error: null,
  updated: false,
}
