import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { initialState, RootReducer } from './reducers'
import { epicMiddleware, RootEpic } from './epics'

const composeEnhancer = composeWithDevTools({
  name: 'tesis-frontend',
})

const Store = createStore(
  RootReducer,
  initialState as any,
  composeEnhancer(applyMiddleware(epicMiddleware))
)

epicMiddleware.run(RootEpic)

export default Store
