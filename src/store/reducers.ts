import { combineReducers } from 'redux'
import { AuthReducer } from './auth'
import { authInitialState, IAuthState } from './auth/types'
// eslint-disable-next-line
import { IndicatorsReducer } from './indicators'
import { IIndicatorsState, indicatorsInitialState } from './indicators/types'
// eslint-disable-next-line
import { StrategicPlanReducer } from './strategic-plan'
import { MacroActivityReducer } from './macroactivity'
import {
  IMacroActivityState,
  macroActivityInitialState,
} from './macroactivity/types'
import { QuizReducer } from './quiz'
import { IQuizState, quizInitialState } from './quiz/types'

import {
  IStrategicPlanState,
  strategicPlanInitialState,
} from './strategic-plan/types'
import {
  IStakeholderState,
  stakeholderInitialState,
} from './stakeholders/types'
// eslint-disable-next-line
import { StakeholderReducer } from './stakeholders'
import { ISponsorshipState, sponsorshipInitialState } from './sponsorship/types'
// eslint-disable-next-line
import { SponsorshipReducer } from './sponsorship'
import { activityInitialState, IActivityState } from './activity/types'
// eslint-disable-next-line
import { ActivityReducer } from './activity'

export interface IState {
  indicators: IIndicatorsState
  quiz: IQuizState
  auth: IAuthState
  strategicPlan: IStrategicPlanState
  macroactivity: IMacroActivityState
  stakeholder: IStakeholderState
  sponsorship: ISponsorshipState
  activity: IActivityState
}

export const initialState: IState = {
  indicators: indicatorsInitialState,
  quiz: quizInitialState,
  auth: authInitialState,
  strategicPlan: strategicPlanInitialState,
  macroactivity: macroActivityInitialState,
  stakeholder: stakeholderInitialState,
  sponsorship: sponsorshipInitialState,
  activity: activityInitialState,
}

export const RootReducer = combineReducers({
  indicators: IndicatorsReducer,
  quiz: QuizReducer,
  auth: AuthReducer,
  strategicPlan: StrategicPlanReducer,
  macroactivity: MacroActivityReducer,
  stakeholder: StakeholderReducer,
  sponsorship: SponsorshipReducer,
  activity: ActivityReducer,
})
