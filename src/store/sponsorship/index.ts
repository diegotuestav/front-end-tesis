import reduceReducers from 'reduce-reducers'
import { combineEpics } from 'redux-observable'
import { addSponsorshipReducer } from './actions/add.actions'
import { sponsorshipLoadReducer } from './actions/load.action'
import { updateSponsorshipReducer } from './actions/update.actions'
// eslint-disable-next-line
import * as epics from './epics'
import { sponsorshipInitialState } from './types'

export const SponsorshipEpics = combineEpics(...Object.values(epics))

export const SponsorshipReducer = reduceReducers(
  sponsorshipInitialState,
  sponsorshipLoadReducer,
  addSponsorshipReducer,
  updateSponsorshipReducer
)
