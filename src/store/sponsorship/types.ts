import { ISponsorhip } from '../../interfaces/sponsorship.interface'

export enum SponsorshipActionTypes {
  LOAD_SPONSORSHIP = '[SPONSORSHIP] Load',
  LOAD_SPONSORSHIP_SUCCESS = '[SPONSORSHIP] Load Success',
  LOAD_SPONSORSHIP_FAIL = '[SPONSORSHIP] Load Fail',

  ADD_SPONSORSHIP = '[SPONSORSHIP] Add',
  ADD_SPONSORSHIP_SUCCESS = '[SPONSORSHIP] Add Success',
  ADD_SPONSORSHIP_FAIL = '[SPONSORSHIP] Add Fail',

  UPDATE_SPONSORSHIP = '[SPONSORSHIP] Update',
  UPDATE_SPONSORSHIP_SUCCESS = '[SPONSORSHIP] Update Success',
  UPDATE_SPONSORSHIP_FAIL = '[SPONSORSHIP] Update Fail',

  CLEAR = '[SPONSORSHIP] Clear',
}

export interface ISponsorshipState {
  sponsorship: ISponsorhip[]
  loading: boolean
  error: any
  added: boolean
  updated: boolean
}

export const sponsorshipInitialState = {
  sponsorship: [],
  loading: false,
  error: null,
  added: false,
  updated: false,
}
