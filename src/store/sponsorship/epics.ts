import { StateObservable } from 'redux-observable'
import { from, Observable, of } from 'rxjs'
import { filter, switchMap, map, catchError } from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
// eslint-disable-next-line
import { IState } from '../reducers'
import { SponsorshipService } from '../services/sponsorship.service'
import { addSponsorshipAction } from './actions/add.actions'
import { sponsorshipLoadAction } from './actions/load.action'
import { updateSponsorshipAction } from './actions/update.actions'

export const sponsorshipsLoadEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(sponsorshipLoadAction.request)),
    switchMap(({ payload }) =>
      from(SponsorshipService.getSponsorships(payload)).pipe(
        map(sponsorshipLoadAction.success),
        catchError((error) => of(sponsorshipLoadAction.failure(error)))
      )
    )
  )

export const addSponsorshipEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(addSponsorshipAction.request)),
    switchMap(({ payload }) =>
      from(SponsorshipService.addSponsorship(payload)).pipe(
        map(addSponsorshipAction.success),
        catchError((error) => of(addSponsorshipAction.failure(error)))
      )
    )
  )

export const addSponsorshipSuccessEpic = (
  action$: Observable<any>,
  state$: StateObservable<IState>
) =>
  action$.pipe(
    filter(isActionOf(addSponsorshipAction.success)),
    map(() =>
      sponsorshipLoadAction.request(state$.value.strategicPlan.strategicPlan.id)
    )
  )

export const updateSponsorshipEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(updateSponsorshipAction.request)),
    switchMap(({ payload }) =>
      from(SponsorshipService.updateSponsorship(payload)).pipe(
        map(updateSponsorshipAction.success),
        catchError((error) => of(updateSponsorshipAction.failure(error)))
      )
    )
  )

export const updateSponsorshipSuccessEpic = (
  action$: Observable<any>,
  state$: StateObservable<IState>
) =>
  action$.pipe(
    filter(isActionOf(updateSponsorshipAction.success)),
    map(() =>
      sponsorshipLoadAction.request(state$.value.strategicPlan.strategicPlan.id)
    )
  )
