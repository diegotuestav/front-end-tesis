import { createAsyncAction, createReducer } from 'typesafe-actions'
import { ISponsorhip } from '../../../interfaces/sponsorship.interface'
import { SponsorshipActionTypes, sponsorshipInitialState } from '../types'

export const sponsorshipLoadAction = createAsyncAction(
  SponsorshipActionTypes.LOAD_SPONSORSHIP,
  SponsorshipActionTypes.LOAD_SPONSORSHIP_SUCCESS,
  SponsorshipActionTypes.LOAD_SPONSORSHIP_FAIL
)<number, ISponsorhip[], any>()

export const sponsorshipLoadReducer = createReducer(sponsorshipInitialState)
  .handleAction(sponsorshipLoadAction.request, (state: any, action: any) => ({
    ...state,
    loading: true,
  }))
  .handleAction(sponsorshipLoadAction.success, (state: any, action: any) => ({
    ...state,
    loading: false,
    sponsorship: action.payload,
  }))
  .handleAction(sponsorshipLoadAction.failure, (state: any, action: any) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
