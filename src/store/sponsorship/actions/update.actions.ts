import { createAsyncAction, createReducer } from 'typesafe-actions'
import { ISponsorhip } from '../../../interfaces/sponsorship.interface'
import { SponsorshipActionTypes, sponsorshipInitialState } from '../types'

export const updateSponsorshipAction = createAsyncAction(
  SponsorshipActionTypes.UPDATE_SPONSORSHIP,
  SponsorshipActionTypes.UPDATE_SPONSORSHIP_SUCCESS,
  SponsorshipActionTypes.UPDATE_SPONSORSHIP_FAIL
)<
  {
    idSponsorship: number
    names: string
    lastnames: string
    email: string
    phone: string
    charge: string
    idRole: number
    idStrategicPlan: number
  },
  boolean,
  any
>()

export const updateSponsorshipReducer = createReducer(sponsorshipInitialState)
  .handleAction(updateSponsorshipAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(updateSponsorshipAction.success, (state, action) => ({
    ...state,
    loading: false,
    updated: true,
  }))
  .handleAction(updateSponsorshipAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
