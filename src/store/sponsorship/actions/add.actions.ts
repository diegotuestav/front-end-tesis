import { createAsyncAction, createReducer } from 'typesafe-actions'
import { ISponsorhip } from '../../../interfaces/sponsorship.interface'
import { SponsorshipActionTypes, sponsorshipInitialState } from '../types'

export const addSponsorshipAction = createAsyncAction(
  SponsorshipActionTypes.ADD_SPONSORSHIP,
  SponsorshipActionTypes.ADD_SPONSORSHIP_SUCCESS,
  SponsorshipActionTypes.ADD_SPONSORSHIP_FAIL
)<
  {
    names: string
    lastnames: string
    email: string
    phone: string
    charge: string
    idRole: number
    idStrategicPlan: number
  },
  boolean,
  any
>()

export const addSponsorshipReducer = createReducer(sponsorshipInitialState)
  .handleAction(addSponsorshipAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(addSponsorshipAction.success, (state, action) => ({
    ...state,
    loading: false,
    added: true,
  }))
  .handleAction(addSponsorshipAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
