import { from, Observable, of } from 'rxjs'
import { filter, switchMap, map, catchError } from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { StateObservable } from 'redux-observable'
// eslint-disable-next-line
import { IState } from '../reducers'
import { StrategicPlanService } from '../services/strategic-plan.service'
import { StrategicPlanListAction } from './actions/list.action'
import { CreateStrategicPlanAction } from './actions/create.action'
import { AddUserStrategicPlanAction } from './actions/add-user.action'
import { StrategicPlanGetAction } from './actions/get.action'
import { StrategicPlanListUsersAction } from './actions/list-users.action'
import { strategicPlanLoadAction } from './actions/load.actions'

export const StrategicPlanListEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(StrategicPlanListAction.request)),
    switchMap(({ payload }) =>
      from(StrategicPlanService.getAll(payload)).pipe(
        map(StrategicPlanListAction.success),
        catchError((error) => of(StrategicPlanListAction.failure(error)))
      )
    )
  )

export const StrategicPlanLoadEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(strategicPlanLoadAction.request)),
    switchMap(() =>
      from(StrategicPlanService.load()).pipe(
        map(({ strategicPlan }) =>
          strategicPlanLoadAction.success(strategicPlan as any)
        ),
        catchError((error) => of(strategicPlanLoadAction.failure(error)))
      )
    )
  )

export const StrategicPlanGetEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(StrategicPlanGetAction.request)),
    switchMap(({ payload }) =>
      from(StrategicPlanService.getStrategicPlan(payload)).pipe(
        map(StrategicPlanGetAction.success),
        catchError((error) => of(StrategicPlanGetAction.failure(error)))
      )
    )
  )

export const StrategicPlanListUsersEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(StrategicPlanListUsersAction.request)),
    switchMap(({ payload }) =>
      from(StrategicPlanService.getUsers(payload)).pipe(
        map(StrategicPlanListUsersAction.success),
        catchError((error) => of(StrategicPlanListUsersAction.failure(error)))
      )
    )
  )

export const CreateStrategicPlanEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(CreateStrategicPlanAction.request)),
    switchMap(({ payload }) =>
      from(StrategicPlanService.createStrategicPlan(payload)).pipe(
        map(CreateStrategicPlanAction.success),
        catchError((error) => of(CreateStrategicPlanAction.failure(error)))
      )
    )
  )

export const StrategicPlanLoadAfterCreateSuccessEpic = (
  action$: Observable<any>,
  state$: StateObservable<IState>
) =>
  action$.pipe(
    filter(isActionOf(CreateStrategicPlanAction.success)),
    map(() => StrategicPlanListAction.request(state$.value.auth.user.id))
  )

export const AddUserStrategicPlanEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(AddUserStrategicPlanAction.request)),
    switchMap(({ payload }) =>
      from(StrategicPlanService.addUser(payload)).pipe(
        map(AddUserStrategicPlanAction.success),
        catchError((error) => of(AddUserStrategicPlanAction.failure(error)))
      )
    )
  )

export const AddUserStrategicPlanAfterSuccessEpic = (
  action$: Observable<any>,
  state$: StateObservable<IState>
) =>
  action$.pipe(
    filter(isActionOf(AddUserStrategicPlanAction.success)),
    map(() =>
      StrategicPlanListUsersAction.request(
        state$.value.strategicPlan.strategicPlan.id
      )
    )
  )
