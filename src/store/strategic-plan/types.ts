import { IStrategicPlan } from '../../interfaces/strategic-plan.interface'
import { IUser } from '../../interfaces/user.interface'

export enum StrategicPlanActionTypes {
  LOAD_STRATEGIC_PLAN = '[STRATEGIC PLAN] Load',
  LOAD_STRATEGIC_PLAN_SUCCESS = '[STRATEGIC PLAN] Load Success',
  LOAD_STRATEGIC_PLAN_FAIL = '[STRATEGIC PLAN] Load Fail',

  LIST_STRATEGIC_PLAN = '[STRATEGIC PLAN] List',
  LIST_STRATEGIC_PLAN_SUCCESS = '[STRATEGIC PLAN] List Success',
  LIST_STRATEGIC_PLAN_FAIL = '[STRATEGIC PLAN] List Fail',

  CREATE_STRATEGIC_PLAN = '[STRATEGIC PLAN] Create',
  CREATE_STRATEGIC_PLAN_SUCCESS = '[STRATEGIC PLAN] Create Success',
  CREATE_STRATEGIC_PLAN_FAIL = '[STRATEGIC PLAN] Create Fail',

  ADD_USER_STRATEGIC_PLAN = '[STRATEGIC PLAN] Add User',
  ADD_USER_STRATEGIC_PLAN_SUCCESS = '[STRATEGIC PLAN] Add User Success',
  ADD_USER_STRATEGIC_PLAN_FAIL = '[STRATEGIC PLAN] Add User Fail',

  GET_STRATEGIC_PLAN = '[STRATEGIC PLAN] Get',
  GET_STRATEGIC_PLAN_SUCCESS = '[STRATEGIC PLAN] Get Success',
  GET_STRATEGIC_PLAN_FAIL = '[STRATEGIC PLAN] Get Fail',

  LIST_USERS_STRATEGIC_PLAN = '[STRATEGIC PLAN] List Users',
  LIST_USERS_STRATEGIC_PLAN_SUCCESS = '[STRATEGIC PLAN] List Users Success',
  LIST_USERS_STRATEGIC_PLAN_FAIL = '[STRATEGIC PLAN] List Users Fail',

  CLEAR = '[STRATEGIC PLAN] Clear',
}

export interface IStrategicPlanState {
  listStrategicPlans: IStrategicPlan[]
  strategicPlan: IStrategicPlan
  users: IUser[]
  loading: boolean
  error: any
  created: boolean
  userAdded: boolean
}

export const strategicPlanInitialState = {
  listStrategicPlans: [],
  strategicPlan: null,
  users: [],
  loading: false,
  error: null,
  created: false,
  userAdded: false,
}
