import { createAsyncAction, createReducer } from 'typesafe-actions'
import { StrategicPlanActionTypes, strategicPlanInitialState } from '../types'
import { IStrategicPlan } from '../../../interfaces/strategic-plan.interface'

export const strategicPlanLoadAction = createAsyncAction(
  StrategicPlanActionTypes.LOAD_STRATEGIC_PLAN,
  StrategicPlanActionTypes.LOAD_STRATEGIC_PLAN_SUCCESS,
  StrategicPlanActionTypes.LOAD_STRATEGIC_PLAN_FAIL
)<void, IStrategicPlan, any>()

export const strategicPlanLoadReducer = createReducer(strategicPlanInitialState)
  .handleAction(strategicPlanLoadAction.request, (state: any, action: any) => ({
    ...state,
    loading: true,
  }))
  .handleAction(strategicPlanLoadAction.success, (state: any, action: any) => ({
    ...state,
    loading: false,
    strategicPlan: action.payload,
  }))
  .handleAction(strategicPlanLoadAction.failure, (state: any, action: any) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
