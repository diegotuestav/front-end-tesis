import { createAsyncAction, createReducer } from 'typesafe-actions'
import { StrategicPlanActionTypes, strategicPlanInitialState } from '../types'
import { IUser } from '../../../interfaces/user.interface'

export const StrategicPlanListUsersAction = createAsyncAction(
  StrategicPlanActionTypes.LIST_USERS_STRATEGIC_PLAN,
  StrategicPlanActionTypes.LIST_USERS_STRATEGIC_PLAN_SUCCESS,
  StrategicPlanActionTypes.LIST_USERS_STRATEGIC_PLAN_FAIL
)<number, IUser[], any>()

export const StrategicPlanListUsersReducer = createReducer(
  strategicPlanInitialState
)
  .handleAction(
    StrategicPlanListUsersAction.request,
    (state: any, action: any) => ({
      ...state,
      loading: true,
    })
  )
  .handleAction(
    StrategicPlanListUsersAction.success,
    (state: any, action: any) => ({
      ...state,
      loading: false,
      users: action.payload,
    })
  )
  .handleAction(
    StrategicPlanListUsersAction.failure,
    (state: any, action: any) => ({
      ...state,
      loading: false,
      error: action.payload,
    })
  )
