import { createAsyncAction, createReducer } from 'typesafe-actions'
import { StrategicPlanActionTypes, strategicPlanInitialState } from '../types'
import { IStrategicPlan } from '../../../interfaces/strategic-plan.interface'

export const StrategicPlanGetAction = createAsyncAction(
  StrategicPlanActionTypes.GET_STRATEGIC_PLAN,
  StrategicPlanActionTypes.GET_STRATEGIC_PLAN_SUCCESS,
  StrategicPlanActionTypes.GET_STRATEGIC_PLAN_FAIL
)<number, IStrategicPlan, any>()

export const StrategicPlanGetReducer = createReducer(strategicPlanInitialState)
  .handleAction(StrategicPlanGetAction.request, (state: any, action: any) => ({
    ...state,
    loading: true,
  }))
  .handleAction(StrategicPlanGetAction.success, (state: any, action: any) => ({
    ...state,
    loading: false,
    strategicPlan: action.payload,
  }))
  .handleAction(StrategicPlanGetAction.failure, (state: any, action: any) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
