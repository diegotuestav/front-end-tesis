import { createAsyncAction, createReducer } from 'typesafe-actions'
import { StrategicPlanActionTypes, strategicPlanInitialState } from '../types'

export const AddUserStrategicPlanAction = createAsyncAction(
  StrategicPlanActionTypes.ADD_USER_STRATEGIC_PLAN,
  StrategicPlanActionTypes.ADD_USER_STRATEGIC_PLAN_SUCCESS,
  StrategicPlanActionTypes.ADD_USER_STRATEGIC_PLAN_FAIL
)<{ idStrategicPlan: number; email: string }, boolean, any>()

export const AddUserStrategicPlanReducer = createReducer(
  strategicPlanInitialState
)
  .handleAction(
    AddUserStrategicPlanAction.request,
    (state: any, action: any) => ({
      ...state,
      loading: true,
    })
  )
  .handleAction(
    AddUserStrategicPlanAction.success,
    (state: any, action: any) => ({
      ...state,
      loading: false,
      userAdded: action.payload,
    })
  )
  .handleAction(
    AddUserStrategicPlanAction.failure,
    (state: any, action: any) => ({
      ...state,
      loading: false,
      error: action.payload,
    })
  )
