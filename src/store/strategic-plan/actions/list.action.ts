import { createAsyncAction, createReducer } from 'typesafe-actions'
import { StrategicPlanActionTypes, strategicPlanInitialState } from '../types'
import { IStrategicPlan } from '../../../interfaces/strategic-plan.interface'

export const StrategicPlanListAction = createAsyncAction(
  StrategicPlanActionTypes.LIST_STRATEGIC_PLAN,
  StrategicPlanActionTypes.LIST_STRATEGIC_PLAN_SUCCESS,
  StrategicPlanActionTypes.LIST_STRATEGIC_PLAN_FAIL
)<number, IStrategicPlan[], any>()

export const StrategicPlanListReducer = createReducer(strategicPlanInitialState)
  .handleAction(StrategicPlanListAction.request, (state: any, action: any) => ({
    ...state,
    loading: true,
  }))
  .handleAction(StrategicPlanListAction.success, (state: any, action: any) => ({
    ...state,
    loading: false,
    listStrategicPlans: action.payload,
  }))
  .handleAction(StrategicPlanListAction.failure, (state: any, action: any) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
