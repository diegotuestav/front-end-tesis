import { createAsyncAction, createReducer } from 'typesafe-actions'
import { StrategicPlanActionTypes, strategicPlanInitialState } from '../types'

export const CreateStrategicPlanAction = createAsyncAction(
  StrategicPlanActionTypes.CREATE_STRATEGIC_PLAN,
  StrategicPlanActionTypes.CREATE_STRATEGIC_PLAN_SUCCESS,
  StrategicPlanActionTypes.CREATE_STRATEGIC_PLAN_FAIL
)<{ idUser: number; organizationName: string }, boolean, any>()

export const CreateStrategicPlanReducer = createReducer(
  strategicPlanInitialState
)
  .handleAction(
    CreateStrategicPlanAction.request,
    (state: any, action: any) => ({
      ...state,
      loading: true,
    })
  )
  .handleAction(
    CreateStrategicPlanAction.success,
    (state: any, action: any) => ({
      ...state,
      loading: false,
      created: action.payload,
    })
  )
  .handleAction(
    CreateStrategicPlanAction.failure,
    (state: any, action: any) => ({
      ...state,
      loading: false,
      error: action.payload,
    })
  )
