import reduceReducers from 'reduce-reducers'
import { combineEpics } from 'redux-observable'
import { AddUserStrategicPlanReducer } from './actions/add-user.action'
import { CreateStrategicPlanReducer } from './actions/create.action'
import { StrategicPlanGetReducer } from './actions/get.action'
import { StrategicPlanListUsersReducer } from './actions/list-users.action'
import { StrategicPlanListReducer } from './actions/list.action'
import { strategicPlanLoadReducer } from './actions/load.actions'
// eslint-disable-next-line
import * as epics from './epics'
import { strategicPlanInitialState } from './types'

export const StrategicPlanEpics = combineEpics(...Object.values(epics))

export const StrategicPlanReducer = reduceReducers(
  strategicPlanInitialState,
  StrategicPlanListReducer,
  CreateStrategicPlanReducer,
  AddUserStrategicPlanReducer,
  StrategicPlanGetReducer,
  StrategicPlanListUsersReducer,
  strategicPlanLoadReducer
)
