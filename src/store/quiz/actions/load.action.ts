import { createAsyncAction, createReducer } from 'typesafe-actions'
import { QuizActionTypes, quizInitialState } from '../types'
import { IQuiz } from '../../../interfaces/quiz.interface'

export const quizLoadAction = createAsyncAction(
  QuizActionTypes.LOAD_QUIZ,
  QuizActionTypes.LOAD_QUIZ_SUCCESS,
  QuizActionTypes.LOAD_QUIZ_FAIL
)<number, IQuiz, any>()

export const quizLoadReducer = createReducer(quizInitialState)
  .handleAction(quizLoadAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(quizLoadAction.success, (state, action) => ({
    ...state,
    loading: false,
    quiz: action.payload,
  }))
  .handleAction(quizLoadAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
