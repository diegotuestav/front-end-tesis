import { createAsyncAction, createReducer } from 'typesafe-actions'
import { IQuestion } from '../../../interfaces/question.interface'
import { QuizActionTypes, quizInitialState } from '../types'

export const updateQuizAction = createAsyncAction(
  QuizActionTypes.UPDATE_QUIZ,
  QuizActionTypes.UPDATE_QUIZ_SUCCESS,
  QuizActionTypes.UPDATE_QUIZ_FAIL
)<
  {
    id_indicator?: number
    title?: string
    introduction?: string
    questions?: IQuestion[]
  },
  boolean,
  any
>()

export const updateQuizReducer = createReducer(quizInitialState)
  .handleAction(updateQuizAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(updateQuizAction.success, (state, action) => ({
    ...state,
    loading: false,
    updated: true,
  }))
  .handleAction(updateQuizAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
