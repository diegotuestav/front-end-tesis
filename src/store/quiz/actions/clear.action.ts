import { createAction, createReducer } from 'typesafe-actions'
import { QuizActionTypes, quizInitialState } from '../types'

export const clearQuizAction = createAction(QuizActionTypes.CLEAR)<void>()

export const clearQuizReducer = createReducer(quizInitialState).handleAction(
  clearQuizAction,
  /* eslint-disable-next-line */
  (state, action) => ({
    ...state,
    updated: false,
  })
)
