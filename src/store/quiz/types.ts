import { IQuiz } from '../../interfaces/quiz.interface'

export enum QuizActionTypes {
  LOAD_QUIZ = '[QUIZ] Load',
  LOAD_QUIZ_SUCCESS = '[QUIZ] Load Success',
  LOAD_QUIZ_FAIL = '[QUIZ] Load Fail',

  UPDATE_QUIZ = '[QUIZ] Update',
  UPDATE_QUIZ_SUCCESS = '[QUIZ] Update Success',
  UPDATE_QUIZ_FAIL = '[QUIZ] Update Fail',

  CLEAR = '[QUIZ] Clear State',
}

export interface IQuizState {
  quiz: IQuiz
  loading: boolean
  error: any
  updated: boolean
}

export const quizInitialState = {
  quiz: null,
  loading: false,
  error: null,
  updated: false,
}
