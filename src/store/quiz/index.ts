import reduceReducers from 'reduce-reducers'
import { combineEpics } from 'redux-observable'
import { quizLoadReducer } from './actions/load.action'
import { updateQuizReducer } from './actions/update.actions'
import * as epics from './epics'
import { quizInitialState } from './types'

export const QuizEpics = combineEpics(...Object.values(epics))

export const QuizReducer = reduceReducers(
  quizInitialState,
  quizLoadReducer,
  updateQuizReducer
)
