import { from, Observable, of } from 'rxjs'
import { filter, switchMap, map, catchError } from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { QuizService } from '../services/quiz.service'
import { clearQuizAction } from './actions/clear.action'
import { quizLoadAction } from './actions/load.action'
import { updateQuizAction } from './actions/update.actions'

export const quizGetEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(quizLoadAction.request)),
    switchMap(({ payload }) =>
      from(QuizService.getQuiz(payload)).pipe(
        map(quizLoadAction.success),
        catchError((error) => of(quizLoadAction.failure(error)))
      )
    )
  )

export const UpdateQuizEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(updateQuizAction.request)),
    switchMap(({ payload }) =>
      from(QuizService.updateQuiz(payload)).pipe(
        map(updateQuizAction.success),
        catchError((error) => of(updateQuizAction.failure(error)))
      )
    )
  )

export const clearQuizEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf([updateQuizAction.success])),
    map(() => clearQuizAction())
  )
