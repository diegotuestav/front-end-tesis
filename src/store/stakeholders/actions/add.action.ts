import { createAsyncAction, createReducer } from 'typesafe-actions'
import { StakeholderActionTypes, stakeholderInitialState } from '../types'
import { IStakeholder } from '../../../interfaces/stakeholder.interface'

export const addStakeholderAction = createAsyncAction(
  StakeholderActionTypes.ADD_STAKEHOLDER,
  StakeholderActionTypes.ADD_STAKEHOLDER_SUCCESS,
  StakeholderActionTypes.ADD_STAKEHOLDER_FAIL
)<
  {
    names: string
    lastnames: string
    email: string
    phone: string
    idProfile: number
    idAdhesion: number
    idCollective: number
    idStrategicPlan: number
  },
  boolean,
  any
>()

export const addStakeholderReducer = createReducer(stakeholderInitialState)
  .handleAction(addStakeholderAction.request, (state, action) => ({
    ...state,
    loading: true,
  }))
  .handleAction(addStakeholderAction.success, (state, action) => ({
    ...state,
    loading: false,
    added: true,
  }))
  .handleAction(addStakeholderAction.failure, (state, action) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
