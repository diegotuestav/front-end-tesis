import { createAsyncAction, createReducer } from 'typesafe-actions'
import { StakeholderActionTypes, stakeholderInitialState } from '../types'
import { ICollective } from '../../../interfaces/collective.interface'

export const collectivesLoadAction = createAsyncAction(
  StakeholderActionTypes.LOAD_COLLECTIVES,
  StakeholderActionTypes.LOAD_COLLECTIVES_SUCCESS,
  StakeholderActionTypes.LOAD_COLLECTIVES_FAIL
)<number, ICollective[], any>()

export const collectivesLoadReducer = createReducer(stakeholderInitialState)
  .handleAction(collectivesLoadAction.request, (state: any, action: any) => ({
    ...state,
    loading: true,
  }))
  .handleAction(collectivesLoadAction.success, (state: any, action: any) => ({
    ...state,
    loading: false,
    collectives: action.payload,
  }))
  .handleAction(collectivesLoadAction.failure, (state: any, action: any) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
