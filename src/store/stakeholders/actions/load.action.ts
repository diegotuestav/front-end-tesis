import { createAsyncAction, createReducer } from 'typesafe-actions'
import { StakeholderActionTypes, stakeholderInitialState } from '../types'
import { IStakeholder } from '../../../interfaces/stakeholder.interface'

export const stakeholdersLoadAction = createAsyncAction(
  StakeholderActionTypes.LOAD_STAKEHOLDER,
  StakeholderActionTypes.LOAD_STAKEHOLDER_SUCCESS,
  StakeholderActionTypes.LOAD_STAKEHOLDER_FAIL
)<number, IStakeholder[], any>()

export const stakeholdersLoadReducer = createReducer(stakeholderInitialState)
  .handleAction(stakeholdersLoadAction.request, (state: any, action: any) => ({
    ...state,
    loading: true,
  }))
  .handleAction(stakeholdersLoadAction.success, (state: any, action: any) => ({
    ...state,
    loading: false,
    stakeholders: action.payload,
  }))
  .handleAction(stakeholdersLoadAction.failure, (state: any, action: any) => ({
    ...state,
    loading: false,
    error: action.payload,
  }))
