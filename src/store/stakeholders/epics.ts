import { StateObservable } from 'redux-observable'
import { from, Observable, of } from 'rxjs'
import { filter, switchMap, map, catchError } from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
// eslint-disable-next-line
import { IState } from '../reducers'
import { StakeholderService } from '../services/stakeholder.service'
import { addStakeholderAction } from './actions/add.action'
import { collectivesLoadAction } from './actions/list-collectives.action'
import { stakeholdersLoadAction } from './actions/load.action'

export const stakeholdersLoadEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(stakeholdersLoadAction.request)),
    switchMap(({ payload }) =>
      from(StakeholderService.getStakeholders(payload)).pipe(
        map(stakeholdersLoadAction.success),
        catchError((error) => of(stakeholdersLoadAction.failure(error)))
      )
    )
  )

export const collectivesLoadEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(collectivesLoadAction.request)),
    switchMap(({ payload }) =>
      from(StakeholderService.getCollectives(payload)).pipe(
        map(collectivesLoadAction.success),
        catchError((error) => of(collectivesLoadAction.failure(error)))
      )
    )
  )
export const addStakeholderEpic = (action$: Observable<any>) =>
  action$.pipe(
    filter(isActionOf(addStakeholderAction.request)),
    switchMap(({ payload }) =>
      from(StakeholderService.addStakeholder(payload)).pipe(
        map(addStakeholderAction.success),
        catchError((error) => of(addStakeholderAction.failure(error)))
      )
    )
  )

export const addStakeholderSuccessEpic = (
  action$: Observable<any>,
  state$: StateObservable<IState>
) =>
  action$.pipe(
    filter(isActionOf(addStakeholderAction.success)),
    map(() =>
      stakeholdersLoadAction.request(
        state$.value.strategicPlan.strategicPlan.id
      )
    )
  )
