import { ICollective } from '../../interfaces/collective.interface'
import { IStakeholder } from '../../interfaces/stakeholder.interface'

export enum StakeholderActionTypes {
  LOAD_STAKEHOLDER = '[STAKEHOLDER] Load',
  LOAD_STAKEHOLDER_SUCCESS = '[STAKEHOLDER] Load Success',
  LOAD_STAKEHOLDER_FAIL = '[STAKEHOLDER] Load Fail',

  LOAD_COLLECTIVES = '[STAKEHOLDER] Load Collectives',
  LOAD_COLLECTIVES_SUCCESS = '[STAKEHOLDER] Load Collectives Success',
  LOAD_COLLECTIVES_FAIL = '[STAKEHOLDER] Load Collectives Fail',

  ADD_STAKEHOLDER = '[STAKEHOLDER] Add',
  ADD_STAKEHOLDER_SUCCESS = '[STAKEHOLDER] Add Success',
  ADD_STAKEHOLDER_FAIL = '[STAKEHOLDER] Add Fail',

  CLEAR = '[STAKEHOLDER] Clear',
}

export interface IStakeholderState {
  stakeholders: IStakeholder[]
  collectives: ICollective[]
  loading: boolean
  error: any
  added: boolean
}

export const stakeholderInitialState = {
  stakeholders: [],
  collectives: [],
  loading: false,
  error: null,
  added: false,
}
