import reduceReducers from 'reduce-reducers'
import { combineEpics } from 'redux-observable'
// eslint-disable-next-line
import * as epics from './epics'
import { stakeholderInitialState } from './types'
import { stakeholdersLoadReducer } from './actions/load.action'
import { addStakeholderReducer } from './actions/add.action'
import { collectivesLoadReducer } from './actions/list-collectives.action'

export const StakeholderEpics = combineEpics(...Object.values(epics))

export const StakeholderReducer = reduceReducers(
  stakeholderInitialState,
  stakeholdersLoadReducer,
  addStakeholderReducer,
  collectivesLoadReducer
)
