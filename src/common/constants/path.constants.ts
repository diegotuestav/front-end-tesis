export const Path = {
  LOGIN: '/login',
  LOGOUT: '/logout',
  BOARD: '/board',
  MACRO1: '/board/macro1',
  MACRO2: '/board/macro2',
  MACRO3: '/board/macro3',
  MACRO4: '/board/macro4',
  MACRO5: '/board/macro5',
  MACRO6: '/board/macro6',
  MACRO7: '/board/macro7',
  MACRO8: '/board/macro8',
  MACRO9: '/board/macro9',
  MACRO10: '/board/macro10',
  MACRO11: '/board/macro11',
  MACRO12: '/board/macro12',
  STRATEGIC_PLAN: '/strategic-plan',

  CALENDAR: '/calendar',

  INDICATORS: '/indicators',
  QUIZ: '/indicators/:id/quiz',
  QUIZ_VIEW: '/indicators/:id/quiz/view',
  QUIZ_RESOLVED: '/indicators/:id/quiz/resolved',

  TEAM: '/team',
}
