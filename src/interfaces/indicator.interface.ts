export interface IIndicator {
  id?: number
  indicator?: string
  description?: string
  period?: string
  goal?: number
  formula?: string
  nameVariable1?: string
  variable1?: number
  variableDescription1?: string
  nameVariable2?: string
  variable2?: number
  variableDescription2?: string
  measureUnit?: string
  result?: number
}

export class IndicatorModel {
  id?: number

  indicator?: string

  description?: string

  period?: string

  goal?: number

  formula?: string

  nameVariable1?: string

  variable1?: number

  variableDescription1?: string

  nameVariable2?: string

  variable2?: number

  variableDescription2?: string

  measureUnit?: string

  result?: number

  constructor(
    id: number,
    indicator: string,
    description: string,
    period: string,
    goal: number,
    formula: string,
    nameVariable1: string,
    variable1: number,
    variableDescription1: string,
    nameVariable2: string,
    variable2: number,
    variableDescription2: string,
    measureUnit: string,
    result: number
  ) {
    this.id = id
    this.indicator = indicator
    this.description = description
    this.period = period
    this.goal = goal
    this.formula = formula
    this.nameVariable1 = nameVariable1
    this.variable1 = variable1
    this.variableDescription1 = variableDescription1
    this.nameVariable2 = nameVariable2
    this.variable2 = variable2
    this.variableDescription2 = variableDescription2
    this.measureUnit = measureUnit
    this.result = result
  }
}
