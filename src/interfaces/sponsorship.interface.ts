export interface ISponsorhip {
  id?: number
  names?: string
  lastnames?: string
  email?: string
  phone?: string
  charge?: string
  role?: string
}

export class SponsorhipModel {
  id: number

  names: string

  lastnames: string

  email: string

  phone: string

  charge?: string

  role?: string

  constructor(
    id: number,
    names: string,
    lastnames: string,
    email: string,
    phone: string,
    charge?: string,
    role?: string
  ) {
    this.id = id
    this.names = names
    this.lastnames = lastnames
    this.email = email
    this.phone = phone
    this.charge = charge
    this.role = role
  }
}
