import { IAlternative } from './alternative.interface'

export interface IQuestion {
  id?: number
  statement?: string
  numQuestion?: number
  score?: number
  alternatives?: IAlternative[]
}

export class QuestionModel {
  id?: number

  statement?: string

  numQuestion?: number

  score?: number

  alternatives?: IAlternative[]

  constructor(
    id: number,
    statement: string,
    numQuestion: number,
    score: number,
    alternatives: IAlternative[] = null
  ) {
    this.id = id
    this.statement = statement
    this.numQuestion = numQuestion
    this.score = score
    this.alternatives = alternatives
  }
}
