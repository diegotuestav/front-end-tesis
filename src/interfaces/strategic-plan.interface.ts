import { IMacroActivity } from './macroactivity.interface'

export interface IStrategicPlan {
  id?: number
  organizationName?: string
  macroActivities?: IMacroActivity[]
}

export class StrategicPlanModel {
  id?: number

  organizationName?: string

  macroActivities?: IMacroActivity[]

  constructor(
    id: number,
    organizationName: string,
    macroActivities: IMacroActivity[] = null
  ) {
    this.id = id
    this.organizationName = organizationName
    this.macroActivities = macroActivities
  }
}
