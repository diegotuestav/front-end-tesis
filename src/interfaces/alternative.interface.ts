export interface IAlternative {
  id?: number
  alternative?: string
  isCorrect?: boolean
}

export class AlternativeModel {
  id?: number

  alternative?: string

  isCorrect?: boolean

  constructor(id: number, alternative: string, isCorrect: boolean = false) {
    this.id = id
    this.alternative = alternative
    this.isCorrect = isCorrect
  }
}
