import { IAdhesion } from './adhesion.interface'
import { ICollective } from './collective.interface'
import { IProfile } from './profile.interface'

export interface IStakeholder {
  id?: number
  names?: string
  lastnames?: string
  email?: string
  phone?: string
  profile?: string
  adhesion?: string
  collective?: string
}

export class StakeholderModel {
  id: number

  names: string

  lastnames: string

  email: string

  phone: string

  profile?: string

  adhesion?: string

  collective?: string

  constructor(
    id: number,
    names: string,
    lastnames: string,
    email: string,
    phone: string,
    profile?: string,
    adhesion?: string,
    collective?: string
  ) {
    this.id = id
    this.names = names
    this.lastnames = lastnames
    this.email = email
    this.phone = phone
    this.profile = profile
    this.adhesion = adhesion
    this.collective = collective
  }
}
