import { IUser } from './user.interface'

export interface IActivity {
  id?: number
  title?: string
  description?: string
  notes?: string
  endDate?: Date
  users?: IUser[]
  finished?: boolean
  document?: Blob[]
}

export class ActivityModel {
  id?: number

  title?: string

  description?: string

  notes?: string

  endDate?: Date

  users?: IUser[]

  finished?: boolean

  document?: Blob[]

  constructor(
    id: number,
    title: string,
    description: string,
    notes: string,
    endDate: Date,
    users: IUser[] = null,
    finished: boolean,
    document: Blob[]
  ) {
    this.id = id
    this.title = title
    this.description = description
    this.notes = notes
    this.endDate = endDate
    this.users = users
    this.finished = finished
    this.document = document
  }
}
