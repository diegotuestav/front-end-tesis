import { IActivity } from './activity.interface'

export interface IMacroActivity {
  id?: number
  activities?: IActivity[]
  blankSpace1?: string
  blankSpace2?: string
  blankSpace3?: string
  blankSpace4?: string
  document1?: Blob
  document2?: Blob
}

export class MacroActivityModel {
  id?: number

  activities?: IActivity[]

  blankSpace1?: string

  blankSpace2?: string

  blankSpace3?: string

  blankSpace4?: string

  document1?: Blob

  document2?: Blob

  constructor(
    id: number,
    activities: IActivity[] = null,
    blankSpace1: string,
    blankSpace2: string,
    blankSpace3: string,
    blankSpace4: string,
    document1: Blob,
    document2: Blob
  ) {
    this.id = id
    this.activities = activities
    this.blankSpace1 = blankSpace1
    this.blankSpace2 = blankSpace2
    this.blankSpace3 = blankSpace3
    this.blankSpace4 = blankSpace4
    this.document1 = document1
    this.document2 = document2
  }
}
