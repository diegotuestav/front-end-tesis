export interface IUser {
  id?: number
  names?: string
  lastnames?: string
  email?: string
  phone?: string
  password?: string
  photo?: Blob
}

export class UserModel {
  id: number

  names: string

  lastnames: string

  email: string

  phone: string

  password: string

  photo?: Blob

  constructor(
    id: number,
    names: string,
    lastnames: string,
    email: string,
    phone: string,
    photo?: Blob
  ) {
    this.id = id
    this.names = names
    this.lastnames = lastnames
    this.email = email
    this.phone = phone
    this.photo = photo
  }
}
