export interface ICollective {
  id?: number
  collective?: string
}

export class CollectiveModel {
  id: number

  collective: string

  constructor(id: number, collective: string) {
    this.id = id
    this.collective = collective
  }
}
