import { IQuestion } from './question.interface'

export interface IQuiz {
  title?: string
  introduction?: string
  questions?: IQuestion[]
}

export class QuizModel {
  title?: string

  introduction?: string

  questions?: IQuestion[]

  constructor(
    title: string,
    introduction: string,
    questions: IQuestion[] = null
  ) {
    this.title = title
    this.introduction = introduction
    this.questions = questions
  }
}
