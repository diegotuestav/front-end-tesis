export interface IProfile {
  id?: number
  profile?: string
}

export class ProfileModel {
  id: number

  profile: string

  constructor(id: number, profile: string) {
    this.id = id
    this.profile = profile
  }
}
