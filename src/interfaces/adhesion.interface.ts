export interface IAdhesion {
  id?: number
  adhesion?: string
}

export class AdhesionModel {
  id: number

  adhesion: string

  constructor(id: number, adhesion: string) {
    this.id = id
    this.adhesion = adhesion
  }
}
