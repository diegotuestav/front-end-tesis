import React, { createContext, ReactNode, useEffect, Suspense } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { IState } from '../store/reducers'
import { Login } from '../screens/login/login'
import { IAuthState, authInitialState } from '../store/auth/types'
import { authLoadAction } from '../store/auth/actions/load.actions'
import { strategicPlanLoadAction } from '../store/strategic-plan/actions/load.actions'

export const AuthContext = createContext<IAuthState>(authInitialState)
export const useAuth = () => React.useContext(AuthContext)

export interface AuthProviderProps {
  children: ReactNode
}

export const AuthProvider = ({ children }: AuthProviderProps) => {
  const dispatch = useDispatch()
  const { user, loading, error } = useSelector((state: IState) => state.auth)

  useEffect(() => {
    dispatch(authLoadAction.request())
  }, [dispatch])

  useEffect(() => {
    dispatch(strategicPlanLoadAction.request())
  }, [dispatch])

  const SuspenseLoading = () => {
    return (
      <div className="suspense-loading">
        <div color="#98a9bc" />
        Cargando página
      </div>
    )
  }

  return (
    <AuthContext.Provider value={{ user, loading, error }}>
      {/* eslint-disable */}
      {loading && !user ? <SuspenseLoading /> : user ? children : children}
    </AuthContext.Provider>
  )
}
