import axios from 'axios'

export const https = axios.create({
  baseURL: 'http://54.161.84.124:8080/',
  /*
  baseURL: 'http://localhost:8080/',
*/
  headers: {
    accept: 'application/json',
  },
})
